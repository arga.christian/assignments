package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class DetailRingkasanMahasiswaGUI {
    public DetailRingkasanMahasiswaGUI(JFrame frame, Mahasiswa mahasiswa, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // panel untuk detail ringkasan mahasiswa
    	JPanel detailMhs = new JPanel();
    	detailMhs.setLayout(new BoxLayout(detailMhs, BoxLayout.Y_AXIS));
    	detailMhs.setBackground(new Color(241, 186, 255));
    	detailMhs.add(Box.createVerticalGlue());
    	JLabel titleLabel = new JLabel();
        titleLabel.setText("Detail Ringkasan Mahasiswa");
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        detailMhs.add(titleLabel);
        detailMhs.add(Box.createRigidArea(new Dimension(0, 20)));
        
        // label yang digunakan untuk detail mahasiswa
        JLabel nama = new JLabel();
        nama.setText("Nama: " + mahasiswa.getNama());
        nama.setAlignmentX(Component.CENTER_ALIGNMENT);
        nama.setFont(SistemAkademikGUI.fontGeneral);
        detailMhs.add(nama);
        detailMhs.add(Box.createRigidArea(new Dimension(0, 20)));
        
        JLabel npm = new JLabel();
        npm.setText("NPM: " + mahasiswa.getNpm());
        npm.setAlignmentX(Component.CENTER_ALIGNMENT);
        npm.setFont(SistemAkademikGUI.fontGeneral);
        detailMhs.add(npm);
        detailMhs.add(Box.createRigidArea(new Dimension(0, 20)));
        
        JLabel jurusan = new JLabel();
        jurusan.setText("Jurusan: " + mahasiswa.getJurusan());
        jurusan.setAlignmentX(Component.CENTER_ALIGNMENT);
        jurusan.setFont(SistemAkademikGUI.fontGeneral);
        detailMhs.add(jurusan);
        detailMhs.add(Box.createRigidArea(new Dimension(0, 20)));
        
        JLabel daftarMatkul = new JLabel();
        daftarMatkul.setText("Daftar Mata Kuliah:");
        daftarMatkul.setAlignmentX(Component.CENTER_ALIGNMENT);
        daftarMatkul.setFont(SistemAkademikGUI.fontGeneral);
        detailMhs.add(daftarMatkul);
        detailMhs.add(Box.createRigidArea(new Dimension(0, 20)));
        
        // mengiterasi nama matkul yang diambil oleh mahasiswa
        if (mahasiswa.getBanyakMatkul() == 0) {
        	JLabel noMatkul = new JLabel();
        	noMatkul.setText("Belum ada mata kuliah yang diambil");
        	noMatkul.setAlignmentX(Component.CENTER_ALIGNMENT);
        	noMatkul.setFont(new Font("Century Gothic", Font.BOLD, 14));
        	detailMhs.add(noMatkul);
        	detailMhs.add(Box.createRigidArea(new Dimension(0, 20)));
        }
        for (int i = 0; i < mahasiswa.getBanyakMatkul();i++) {
        	JLabel dftrMtkl = new JLabel();
            dftrMtkl.setText(mahasiswa.getMataKuliah()[i].getNama());
            dftrMtkl.setAlignmentX(Component.CENTER_ALIGNMENT);
            dftrMtkl.setFont(new Font("Century Gothic", Font.BOLD, 14));
            dftrMtkl.setAlignmentX(Component.CENTER_ALIGNMENT);
            detailMhs.add(dftrMtkl);
            detailMhs.add(Box.createRigidArea(new Dimension(0, 20)));
        } 
        
        JLabel totSks = new JLabel();
        totSks.setText("Total SKS: " + mahasiswa.getTotalSKS());
        totSks.setAlignmentX(Component.CENTER_ALIGNMENT);
        totSks.setFont(SistemAkademikGUI.fontGeneral);
        detailMhs.add(totSks);
        detailMhs.add(Box.createRigidArea(new Dimension(0, 20)));
        
        JLabel irs = new JLabel();
        irs.setText("Hasil Pengecekan IRS:");
        irs.setAlignmentX(Component.CENTER_ALIGNMENT);
        irs.setFont(SistemAkademikGUI.fontGeneral);
        detailMhs.add(irs);
        detailMhs.add(Box.createRigidArea(new Dimension(0, 20)));
         
        // menginterasi masalah irs dari mahasiswa tersebut
        if (mahasiswa.getBanyakMasalahIRS() == 0) {
        	JLabel noProblem = new JLabel();
        	noProblem.setText("IRS tidak bermasalah");
        	noProblem.setAlignmentX(Component.CENTER_ALIGNMENT);
        	noProblem.setFont(new Font("Century Gothic", Font.BOLD, 14));
        	detailMhs.add(noProblem);
        	detailMhs.add(Box.createRigidArea(new Dimension(0, 20)));
        } else {
        	for (int i = 0; i < mahasiswa.getBanyakMasalahIRS(); i++) {
            	JLabel masalah = new JLabel();
                masalah.setText(mahasiswa.getMasalahIRS()[i]);
                masalah.setAlignmentX(Component.CENTER_ALIGNMENT);
                masalah.setFont(new Font("Century Gothic", Font.BOLD, 14));
                masalah.setAlignmentX(Component.CENTER_ALIGNMENT);
                detailMhs.add(masalah);
                detailMhs.add(Box.createRigidArea(new Dimension(0, 20)));
            }
        }
        
        JButton buttonSelesai = new JButton("Selesai");
        buttonSelesai.setFont(SistemAkademikGUI.fontGeneral);
        buttonSelesai.setBackground(new Color(10, 196 , 196));
        buttonSelesai.setForeground(new Color(255, 255, 255));
        buttonSelesai.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        //action listener
        buttonSelesai.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		frame.getContentPane().removeAll();
				frame.repaint();
				new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
		        frame.setVisible(true);
        	}
        });
        
        detailMhs.add(buttonSelesai);
        detailMhs.add(Box.createRigidArea(new Dimension(0, 20)));
        detailMhs.add(Box.createVerticalGlue());
        
        frame.add(detailMhs);
    }	
}
