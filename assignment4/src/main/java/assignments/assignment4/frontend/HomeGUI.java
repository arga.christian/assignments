package assignments.assignment4.frontend;

import javax.swing.plaf.basic.BasicButtonListener;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class HomeGUI {
    public HomeGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
    	
    	//panel untuk menampung component dalam home
    	JPanel panel = new JPanel();
    	panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
    	panel.setBackground(new Color(241, 186, 255));
    	panel.add(Box.createVerticalGlue());
    	JLabel titleLabel = new JLabel();
        titleLabel.setText("Selamat datang di Sistem Akademik");
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        
        
        // membuat component yang diperlukan di halaman home
        // membuat button untuk ke menu tambahmahasiswa
    	JButton addMahasiswaButton = new JButton("Tambah Mahasiswa");
    	addMahasiswaButton.setBackground(new Color(10, 196 , 196));
    	addMahasiswaButton.setForeground(new Color(255, 255, 255));
    	addMahasiswaButton.setAlignmentX(Component.CENTER_ALIGNMENT);
    	addMahasiswaButton.setFont(SistemAkademikGUI.fontGeneral);
    	
    	// membuat button untuk ke menu tambahmatakuliah
        JButton addMatakuliahButton = new JButton("Tambah Mata Kuliah");
        addMatakuliahButton.setBackground(new Color(10, 196, 196));
        addMatakuliahButton.setForeground(new Color(255, 255, 255));
        addMatakuliahButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        addMatakuliahButton.setFont(SistemAkademikGUI.fontGeneral);
        
        // membuat button untuk ke menu tambahirs
        JButton addIRSButton = new JButton("Tambah IRS");
        addIRSButton.setBackground(new Color(10, 196, 196));
        addIRSButton.setForeground(new Color(255, 255, 255));
        addIRSButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        addIRSButton.setFont(SistemAkademikGUI.fontGeneral);
        
        // membuat button untuk ke menu deleteirs
        JButton deleteIRSButton = new JButton("Hapus IRS");
        deleteIRSButton.setBackground(new Color(10, 196 , 196));
        deleteIRSButton.setForeground(new Color(255, 255, 255));
        deleteIRSButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        deleteIRSButton.setFont(SistemAkademikGUI.fontGeneral);
        
        // membuat button untuk ke ringkasan mahasiswa
        JButton ringkasanMahasiswaButton = new JButton("Lihat Ringkasan Mahasiswa");
        ringkasanMahasiswaButton.setBackground(new Color(10, 196, 196));
        ringkasanMahasiswaButton.setForeground(new Color(255, 255, 255));
        ringkasanMahasiswaButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        ringkasanMahasiswaButton.setFont(SistemAkademikGUI.fontGeneral);
        
        // membuat button untuk ke menu ringkasan matakuliah
        JButton ringkasanMatakuliahButton = new JButton("Lihat Ringkasan Mata Kuliah");
        ringkasanMatakuliahButton.setBackground(new Color(10, 196, 196));
        ringkasanMatakuliahButton.setForeground(new Color(255, 255, 255));
        ringkasanMatakuliahButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        ringkasanMatakuliahButton.setFont(SistemAkademikGUI.fontGeneral);
        
        // menambah actionlistener
        addMahasiswaButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				frame.getContentPane().removeAll();
				frame.repaint();
				new TambahMahasiswaGUI(frame, daftarMahasiswa, daftarMataKuliah);
				frame.setVisible(true);
			}});
        
        
        addMatakuliahButton.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		frame.getContentPane().removeAll();
				frame.repaint();
        		new TambahMataKuliahGUI(frame, daftarMahasiswa, daftarMataKuliah);
        		frame.setVisible(true);
        	}
        });
        
        addIRSButton.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		frame.getContentPane().removeAll();
				frame.repaint();
        		new TambahIRSGUI(frame, daftarMahasiswa, daftarMataKuliah);
        		frame.setVisible(true);
        	}
        });
        
        deleteIRSButton.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		frame.getContentPane().removeAll();
				frame.repaint();
        		new HapusIRSGUI(frame, daftarMahasiswa, daftarMataKuliah);
        		frame.setVisible(true);
        	}
        });
        
        ringkasanMahasiswaButton.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		frame.getContentPane().removeAll();
				frame.repaint();
        		new RingkasanMahasiswaGUI(frame, daftarMahasiswa, daftarMataKuliah);
        		frame.setVisible(true);
        	}
        });
        
        ringkasanMatakuliahButton.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		frame.getContentPane().removeAll();
				frame.repaint();
        		new RingkasanMataKuliahGUI(frame, daftarMahasiswa, daftarMataKuliah);
        		frame.setVisible(true);
        	}
        });
        
        //memasukkan button ke dalam panel
        panel.add(titleLabel);
        panel.add(Box.createRigidArea(new Dimension(0, 20)));
        panel.add(addMahasiswaButton);
        panel.add(Box.createRigidArea(new Dimension(0, 20)));
        panel.add(addMatakuliahButton);
        panel.add(Box.createRigidArea(new Dimension(0, 20)));
        panel.add(addIRSButton);
        panel.add(Box.createRigidArea(new Dimension(0, 20)));
        panel.add(deleteIRSButton);
        panel.add(Box.createRigidArea(new Dimension(0, 20)));
        panel.add(ringkasanMahasiswaButton);
        panel.add(Box.createRigidArea(new Dimension(0, 20)));
        panel.add(ringkasanMatakuliahButton);
        panel.add(Box.createRigidArea(new Dimension(0, 20)));
        panel.add(Box.createVerticalGlue());
        
        //memasukkan panel ke dalam frame
        frame.add(panel);
    }
    
    void buttonAnimation(JButton button) {
    	button.addMouseListener(new MouseAdapter(){
    		@Override
    		public void mouseClicked(MouseEvent e) {
    			
    			button.setBackground(new Color(0, 0, 0));
    			button.setForeground(new Color(255, 255, 255));
    		}
    	});
    	
    	button.addMouseListener(new MouseAdapter() {
    		public void mouseExited(MouseEvent e) {
    			
    			button.setBackground(new Color(10, 196 , 196));
    			button.setForeground(new Color(255, 255, 255));
    		}
    	});
    	
    }
}
