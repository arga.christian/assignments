package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class RingkasanMahasiswaGUI {

    public RingkasanMahasiswaGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // panel untuk component ringkasan mahasiswa
    	JPanel ringkasanMhs = new JPanel();
    	ringkasanMhs.setLayout(new BoxLayout(ringkasanMhs, BoxLayout.Y_AXIS));
    	ringkasanMhs.setBackground(new Color(241, 186, 255));
    	ringkasanMhs.add(Box.createVerticalGlue());
    	JLabel titleLabel = new JLabel();
    	titleLabel.setText("Ringkasan Mahasiswa");
    	titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
    	titleLabel.setFont(SistemAkademikGUI.fontTitle);
    	
        JLabel chooseNpm = new JLabel();
        chooseNpm.setText("Pilih NPM");
        chooseNpm.setAlignmentX(Component.CENTER_ALIGNMENT);
        chooseNpm.setFont(SistemAkademikGUI.fontGeneral);
        
        // combobox untuk memilih npm mahasiswa
        JComboBox npmChooser = new JComboBox(sortingNpm(daftarMahasiswa));
        npmChooser.setMaximumSize(new Dimension(200, 0));
        npmChooser.setFont(SistemAkademikGUI.fontGeneral);
        npmChooser.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        // button untuk melihat ringkasan matakuliah
        JButton lihatButton = new JButton("lihat");
        lihatButton.setFont(SistemAkademikGUI.fontGeneral);
        lihatButton.setBackground(new Color(10, 196 , 196));
        lihatButton.setForeground(new Color(255, 255, 255));
        lihatButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        // button untuk kembali ke home
        JButton kembaliButton = new JButton("Kembali");
        kembaliButton.setBackground(new Color(10, 196 , 196));
        kembaliButton.setFont(SistemAkademikGUI.fontGeneral);
        kembaliButton.setForeground(new Color(255, 255, 255));
        kembaliButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        //actionlistener
        lihatButton.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		String npm = (String) npmChooser.getSelectedItem();
        		if (npm == null) {
        			JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
        		} else {
        			Mahasiswa mhs = getMahasiswa(npm, daftarMahasiswa);
        			frame.getContentPane().removeAll();
        			frame.repaint();
        			new DetailRingkasanMahasiswaGUI(frame, mhs, daftarMahasiswa, daftarMataKuliah);
        			frame.setVisible(true);
        		}
        	}
        });
        
        kembaliButton.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		frame.getContentPane().removeAll();
				frame.repaint();
				new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
		        frame.setVisible(true);
        	}
        });
        
        // memasukkan component yang ada ke dalam panel
        ringkasanMhs.add(titleLabel);
        ringkasanMhs.add(Box.createRigidArea(new Dimension(0, 20)));
        ringkasanMhs.add(chooseNpm);
        ringkasanMhs.add(Box.createRigidArea(new Dimension(0, 20)));
        ringkasanMhs.add(npmChooser);
        ringkasanMhs.add(Box.createRigidArea(new Dimension(0, 20)));
        ringkasanMhs.add(lihatButton);
        ringkasanMhs.add(Box.createRigidArea(new Dimension(0, 20)));
        ringkasanMhs.add(kembaliButton);
        ringkasanMhs.add(Box.createRigidArea(new Dimension(0, 20)));
        ringkasanMhs.add(Box.createVerticalGlue());
        
        frame.add(ringkasanMhs);
    }


    private Mahasiswa getMahasiswa(String npm, ArrayList<Mahasiswa> daftarMahasiswa) {
        for (Mahasiswa mahasiswa : daftarMahasiswa) {
            if (Long.toString(mahasiswa.getNpm()).equals(npm)){
                return mahasiswa;
            }
        }
        return null;
    }
    
    private String[] sortingNpm(ArrayList<Mahasiswa> listDaftarMahasiswa) {
    	String[] npmMahasiswa = new String[listDaftarMahasiswa.size()];
    	int helper = 0;
    	for (Mahasiswa mhs : listDaftarMahasiswa) {
    		npmMahasiswa[helper++] = Long.toString(mhs.getNpm());
    	}for (int i = 0; i < npmMahasiswa.length; i++) {
    		for (int j = i+1; j < npmMahasiswa.length; j++) {
    			if (npmMahasiswa[i].compareTo(npmMahasiswa[j]) > 0) {
                    String min = npmMahasiswa[j];
                    npmMahasiswa[j] = npmMahasiswa[i];
                    npmMahasiswa[i] = min;
                }
    		}
    	}
    	return npmMahasiswa;
    }
}
