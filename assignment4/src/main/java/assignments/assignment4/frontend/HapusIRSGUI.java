package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class HapusIRSGUI {

    public HapusIRSGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
    	
    	// panel untuk component hapus irs
    	JPanel deleteIRS = new JPanel();
    	deleteIRS.setLayout(new BoxLayout(deleteIRS, BoxLayout.Y_AXIS));
    	deleteIRS.setBackground(new Color(241, 186, 255));
    	deleteIRS.add(Box.createVerticalGlue());
        JLabel sambutan = new JLabel();
        sambutan.setText("Hapus IRS");
        sambutan.setAlignmentX(Component.CENTER_ALIGNMENT);
        sambutan.setFont(SistemAkademikGUI.fontTitle);
        
        JLabel chooseNpm = new JLabel();
        chooseNpm.setText("Pilih NPM");
        chooseNpm.setAlignmentX(Component.CENTER_ALIGNMENT);
        chooseNpm.setFont(SistemAkademikGUI.fontGeneral);
        
        // combobox untuk memilih npm mahasiswa
        JComboBox npmChooser = new JComboBox(sortingNpm(daftarMahasiswa));
        npmChooser.setMaximumSize(new Dimension(200, 0));
        npmChooser.setFont(SistemAkademikGUI.fontGeneral);
        npmChooser.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        JLabel chooseMtkl = new JLabel();
        chooseMtkl.setText("Pilih Nama Matkul");
        chooseMtkl.setAlignmentX(Component.CENTER_ALIGNMENT);
        chooseMtkl.setFont(SistemAkademikGUI.fontGeneral);
        
        // combobox untuk memilih matakuliah
        JComboBox mtklChooser = new JComboBox(sortingMtkl(daftarMataKuliah));
        mtklChooser.setMaximumSize(new Dimension(200, 0));
        mtklChooser.setFont(SistemAkademikGUI.fontGeneral);
        mtklChooser.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        // button untuk menghapus matakuliah dari irs mahasiswa
        JButton hapusButton = new JButton("Hapus");
        hapusButton.setBackground(new Color(10, 196 , 196));
        hapusButton.setFont(SistemAkademikGUI.fontGeneral);
        hapusButton.setForeground(new Color(255, 255, 255));
        hapusButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        // button untuk kembali ke home
        JButton kembaliButton = new JButton("Kembali");
        kembaliButton.setBackground(new Color(10, 196 , 196));
        kembaliButton.setFont(SistemAkademikGUI.fontGeneral);
        kembaliButton.setForeground(new Color(255, 255, 255));
        kembaliButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        //action listener
        hapusButton.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		Long longNpm = Long.parseLong((String) npmChooser.getSelectedItem());
        		String namaMtkl = (String) mtklChooser.getSelectedItem();
        		if (longNpm == null || namaMtkl == null) {
        			JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
        		} else {
        			Mahasiswa mhs = getMahasiswa(longNpm, daftarMahasiswa);
        			MataKuliah matkul = getMataKuliah(namaMtkl, daftarMataKuliah);
        			JOptionPane.showMessageDialog(frame, mhs.dropMatkul(matkul));
        		}
        	}
        });
        
        kembaliButton.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		frame.getContentPane().removeAll();
				frame.repaint();
				new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
		        frame.setVisible(true);
        	}
        });
        
        // memasukkan component ke dalam panel
        deleteIRS.add(sambutan);
        deleteIRS.add(Box.createRigidArea(new Dimension(0, 20)));
        deleteIRS.add(chooseNpm);
        deleteIRS.add(Box.createRigidArea(new Dimension(0, 20)));
        deleteIRS.add(npmChooser);
        deleteIRS.add(Box.createRigidArea(new Dimension(0, 20)));
        deleteIRS.add(chooseMtkl);
        deleteIRS.add(Box.createRigidArea(new Dimension(0, 20)));
        deleteIRS.add(mtklChooser);
        deleteIRS.add(Box.createRigidArea(new Dimension(0, 20)));
        deleteIRS.add(hapusButton);
        deleteIRS.add(Box.createRigidArea(new Dimension(0, 20)));
        deleteIRS.add(kembaliButton);
        deleteIRS.add(Box.createRigidArea(new Dimension(0, 20)));
        deleteIRS.add(Box.createVerticalGlue());
        
        frame.add(deleteIRS);
    }

    private MataKuliah getMataKuliah(String nama, ArrayList<MataKuliah> daftarMataKuliah) {

        for (MataKuliah mataKuliah : daftarMataKuliah) {
            if (mataKuliah.getNama().equals(nama)){
                return mataKuliah;
            }
        }
        return null;
    }

    private Mahasiswa getMahasiswa(long npm, ArrayList<Mahasiswa> daftarMahasiswa) {

        for (Mahasiswa mahasiswa : daftarMahasiswa) {
            if (mahasiswa.getNpm() == npm){
                return mahasiswa;
            }
        }
        return null;
    }
    
    private String[] sortingMtkl(ArrayList<MataKuliah> listDaftarMataKuliah) {
    	String[] namaMatkul = new String[listDaftarMataKuliah.size()];
    	int helper = 0;
    	for (MataKuliah mtkl : listDaftarMataKuliah) {
    		namaMatkul[helper++] = mtkl.getNama();
    	}for (int i = 0; i < namaMatkul.length; i ++) {
    		for (int j = i+1; j < namaMatkul.length; j++) {
    			if (namaMatkul[i].compareTo(namaMatkul[j]) > 0) {
                    String min = namaMatkul[j];
                    namaMatkul[j] = namaMatkul[i];
                    namaMatkul[i] = min;
                }
    		}
    	}
    	return namaMatkul;
    }
    private String[] sortingNpm(ArrayList<Mahasiswa> listDaftarMahasiswa) {
    	String[] npmMahasiswa = new String[listDaftarMahasiswa.size()];
    	int helper = 0;
    	for (Mahasiswa mhs : listDaftarMahasiswa) {
    		npmMahasiswa[helper++] = Long.toString(mhs.getNpm());
    	}for (int i = 0; i < npmMahasiswa.length; i++) {
    		for (int j = i+1; j < npmMahasiswa.length; j++) {
    			if (npmMahasiswa[i].compareTo(npmMahasiswa[j]) > 0) {
                    String min = npmMahasiswa[j];
                    npmMahasiswa[j] = npmMahasiswa[i];
                    npmMahasiswa[i] = min;
                }
    		}
    	}
    	return npmMahasiswa;
    }
}
