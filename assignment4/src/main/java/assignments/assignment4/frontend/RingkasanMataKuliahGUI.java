package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class RingkasanMataKuliahGUI {

    public RingkasanMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
    	
    	//panel untuk component ringkasanmatakuliah
    	JPanel ringkasanMtkl = new JPanel();
    	ringkasanMtkl.setLayout(new BoxLayout(ringkasanMtkl, BoxLayout.Y_AXIS));
    	ringkasanMtkl.setBackground(new Color(241, 186, 255));
    	ringkasanMtkl.add(Box.createVerticalGlue());
    	JLabel titleLabel = new JLabel();
    	titleLabel.setText("Ringkasan Mata Kuliah");
    	titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
    	titleLabel.setFont(SistemAkademikGUI.fontTitle);
    	
        JLabel chooseMtkl = new JLabel();
        chooseMtkl.setText("Pilih Nama Matkul");
        chooseMtkl.setAlignmentX(Component.CENTER_ALIGNMENT);
        chooseMtkl.setFont(SistemAkademikGUI.fontGeneral);
        
        // combobox untuk memilih matakuliah
        JComboBox mtklChooser = new JComboBox(sortingMtkl(daftarMataKuliah));
        mtklChooser.setMaximumSize(new Dimension(200, 0));
        mtklChooser.setFont(SistemAkademikGUI.fontGeneral);
        mtklChooser.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        // button untuk melihat ringkasan matakuliah yang dipilih
        JButton lihatButton = new JButton("lihat");
        lihatButton.setFont(SistemAkademikGUI.fontGeneral);
        lihatButton.setBackground(new Color(10, 196 , 196));
        lihatButton.setForeground(new Color(255, 255, 255));
        lihatButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        // button untuk kembali ke home
        JButton kembaliButton = new JButton("Kembali");
        kembaliButton.setBackground(new Color(10, 196 , 196));
        kembaliButton.setFont(SistemAkademikGUI.fontGeneral);
        kembaliButton.setForeground(new Color(255, 255, 255));
        kembaliButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        //actionlistener
        lihatButton.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		String matkul = (String) mtklChooser.getSelectedItem();
        		if (matkul == null) {
        			JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
        		} else {
        			MataKuliah mtkl = getMataKuliah(matkul, daftarMataKuliah);
        			frame.getContentPane().removeAll();
        			frame.repaint();
        			new DetailRingkasanMataKuliahGUI(frame, mtkl, daftarMahasiswa, daftarMataKuliah);
        			frame.setVisible(true);
        		}
        	}
        });
        
        kembaliButton.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		frame.getContentPane().removeAll();
				frame.repaint();
				new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
		        frame.setVisible(true);
        	}
        });
        
        // memasukkan component ke dalam panel
        ringkasanMtkl.add(titleLabel);
        ringkasanMtkl.add(Box.createRigidArea(new Dimension(0, 20)));
        ringkasanMtkl.add(chooseMtkl);
        ringkasanMtkl.add(Box.createRigidArea(new Dimension(0, 20)));
        ringkasanMtkl.add(mtklChooser);
        ringkasanMtkl.add(Box.createRigidArea(new Dimension(0, 20)));
        ringkasanMtkl.add(lihatButton);
        ringkasanMtkl.add(Box.createRigidArea(new Dimension(0, 20)));
        ringkasanMtkl.add(kembaliButton);
        ringkasanMtkl.add(Box.createRigidArea(new Dimension(0, 20)));
        ringkasanMtkl.add(Box.createVerticalGlue());
        
        frame.add(ringkasanMtkl);
    }


    private MataKuliah getMataKuliah(String nama, ArrayList<MataKuliah> daftarMataKuliah) {

        for (MataKuliah mataKuliah : daftarMataKuliah) {
            if (mataKuliah.getNama().equals(nama)){
                return mataKuliah;
            }
        }
        return null;
    }
    
    private String[] sortingMtkl(ArrayList<MataKuliah> listDaftarMataKuliah) {
    	String[] namaMatkul = new String[listDaftarMataKuliah.size()];
    	int helper = 0;
    	for (MataKuliah mtkl : listDaftarMataKuliah) {
    		namaMatkul[helper++] = mtkl.getNama();
    	}for (int i = 0; i < namaMatkul.length; i ++) {
    		for (int j = i+1; j < namaMatkul.length; j++) {
    			if (namaMatkul[i].compareTo(namaMatkul[j]) > 0) {
                    String min = namaMatkul[j];
                    namaMatkul[j] = namaMatkul[i];
                    namaMatkul[i] = min;
                }
    		}
    	}
    	return namaMatkul;
    }
}
