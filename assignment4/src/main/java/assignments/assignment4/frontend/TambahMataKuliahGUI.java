package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahMataKuliahGUI{

    public TambahMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        
        // membuat panel untuk component tambah matakuliah
    	JPanel panel = new JPanel();
    	panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
    	panel.setBackground(new Color(241, 186, 255));
    	panel.add(Box.createVerticalGlue());
    	JLabel titleLabel = new JLabel();
        titleLabel.setText("Tambah Mata Kuliah");
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        
        JLabel kodeMtkl = new JLabel();
        kodeMtkl.setText("Kode Mata Kuliah:");
        kodeMtkl.setFont(SistemAkademikGUI.fontGeneral);
        kodeMtkl.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        // textfield untuk kode matkul
        JTextField kodeField = new JTextField();
        kodeField.setFont(SistemAkademikGUI.fontGeneral);
        kodeField.setAlignmentX(Component.CENTER_ALIGNMENT);
        kodeField.setMaximumSize(new Dimension(200,0));
        
        JLabel namaMtkl = new JLabel();
        namaMtkl.setText("Nama Mata Kuliah: ");
        namaMtkl.setAlignmentX(Component.CENTER_ALIGNMENT);
        namaMtkl.setFont(SistemAkademikGUI.fontGeneral);
        
        // textfield untuk nama matkul
        JTextField namaField = new JTextField();
        namaField.setAlignmentX(Component.CENTER_ALIGNMENT);
        namaField.setMaximumSize(new Dimension(200,0));
        namaField.setFont(SistemAkademikGUI.fontGeneral);
        
        JLabel sksMtkl = new JLabel();
        sksMtkl.setText("SKS:");
        sksMtkl.setAlignmentX(Component.CENTER_ALIGNMENT);
        sksMtkl.setFont(SistemAkademikGUI.fontGeneral);
        
        // textfield untuk sks matkul
        JTextField sksField = new JTextField();
        sksField.setAlignmentX(Component.CENTER_ALIGNMENT);
        sksField.setMaximumSize(new Dimension(200,0));
        sksField.setFont(SistemAkademikGUI.fontGeneral);
        
        JLabel kapasitasMtkl = new JLabel();
        kapasitasMtkl.setText("Kapasitas:");
        kapasitasMtkl.setAlignmentX(Component.CENTER_ALIGNMENT);
        kapasitasMtkl.setFont(SistemAkademikGUI.fontGeneral);
        
        //  textfield untuk kapasitas matkul
        JTextField kapasitasField = new JTextField();
        kapasitasField.setAlignmentX(Component.CENTER_ALIGNMENT);
        kapasitasField.setMaximumSize(new Dimension(200,0));
        kapasitasField.setFont(SistemAkademikGUI.fontGeneral);
        
        // button untuk menambah matakuliah
        JButton tambahButton = new JButton("Tambahkan");
        tambahButton.setBackground(new Color(10, 196 , 196));
        tambahButton.setFont(SistemAkademikGUI.fontGeneral);
        tambahButton.setForeground(new Color(255, 255, 255));
        tambahButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        //button untuk kembali home gui
        JButton kembaliButton = new JButton("Kembali");
        kembaliButton.setBackground(new Color(10, 196 , 196));
        kembaliButton.setFont(SistemAkademikGUI.fontGeneral);
        kembaliButton.setForeground(new Color(255, 255, 255));
        kembaliButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        //action listener
        tambahButton.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		String kodeMtkl = kodeField.getText();
        		String namaMtkl = namaField.getText();
        		String sksMtkl = sksField.getText();
        		String kapasitasMtkl = kapasitasField.getText();
        		if (kodeMtkl.equals("") || namaMtkl.equals("") || sksMtkl.equals("") || kapasitasMtkl.equals("")) {
        			JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
        			kodeField.setText("");
        			namaField.setText("");
        			sksField.setText("");
        			kapasitasField.setText("");
        		} else if (namaChecker(namaMtkl, daftarMataKuliah)) {
        			int intSks = Integer.parseInt(sksMtkl);
        			int intKapasitas = Integer.parseInt(kapasitasMtkl);
        			MataKuliah matkul = new MataKuliah(kodeMtkl, namaMtkl, intSks, intKapasitas);
        			daftarMataKuliah.add(matkul);
        			JOptionPane.showMessageDialog(frame, String.format("Mata kuliah %s berhasil ditambahkan", matkul.getNama()));
        			kodeField.setText("");
        			namaField.setText("");
        			sksField.setText("");
        			kapasitasField.setText("");
        		} else {
        			JOptionPane.showMessageDialog(frame, String.format("Mata kuliah %s sudah pernah ditambahkan sebelumnya", namaMtkl));
        			kodeField.setText("");
        			namaField.setText("");
        			sksField.setText("");
        			kapasitasField.setText("");
        		}
        	}
        });
        
        kembaliButton.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		frame.getContentPane().removeAll();
				frame.repaint();
				new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
		        frame.setVisible(true);
        	}
        });
        
        // menambahkan component ke panel
        panel.add(titleLabel);
        panel.add(Box.createRigidArea(new Dimension(0, 20)));
        panel.add(kodeMtkl);
        panel.add(Box.createRigidArea(new Dimension(0, 20)));
        panel.add(kodeField);
        panel.add(Box.createRigidArea(new Dimension(0, 20)));
        panel.add(namaMtkl);
        panel.add(Box.createRigidArea(new Dimension(0, 20)));
        panel.add(namaField);
        panel.add(Box.createRigidArea(new Dimension(0, 20)));
        panel.add(sksMtkl);
        panel.add(Box.createRigidArea(new Dimension(0, 20)));
        panel.add(sksField);
        panel.add(Box.createRigidArea(new Dimension(0, 20)));
        panel.add(kapasitasMtkl);
        panel.add(Box.createRigidArea(new Dimension(0, 20)));
        panel.add(kapasitasField);
        panel.add(Box.createRigidArea(new Dimension(0, 20)));
        panel.add(tambahButton);
        panel.add(Box.createRigidArea(new Dimension(0, 20)));
        panel.add(kembaliButton);
        panel.add(Box.createRigidArea(new Dimension(0, 20)));
        panel.add(Box.createVerticalGlue());
        
        frame.add(panel);

    }
    public boolean namaChecker(String nama, ArrayList<MataKuliah> daftarMataKuliah) {
    	for (MataKuliah matkul : daftarMataKuliah) {
    		if (matkul.getNama().equals(nama)) {
    			return false;
    		}
    	}
    	return true;
    }
    
}
