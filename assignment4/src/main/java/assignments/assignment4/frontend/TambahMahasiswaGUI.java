package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahMahasiswaGUI{

    public TambahMahasiswaGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
      
        //membuat panel untuk menanmpung component yang diperlukan
    	JPanel panel = new JPanel();
    	panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
    	panel.setBackground(new Color(241, 186, 255));
    	panel.add(Box.createVerticalGlue());
    	JLabel titleLabel = new JLabel();
        titleLabel.setText("Tambah Mahasiswa");
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        
        //component yang diperlukan untuk mahasiswa
        JLabel nama = new JLabel();
        nama.setText("Nama:");
        nama.setFont(SistemAkademikGUI.fontGeneral);
        nama.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        //textfield untuk nama
        JTextField namaField = new JTextField();
        namaField.setAlignmentX(Component.CENTER_ALIGNMENT);
        namaField.setFont(SistemAkademikGUI.fontGeneral);
        namaField.setMaximumSize(new Dimension(200,0));
        
        JLabel npm = new JLabel();
        npm.setText("NPM: ");
        npm.setAlignmentX(Component.CENTER_ALIGNMENT);
        npm.setFont(SistemAkademikGUI.fontGeneral);
        
        //textfield untuk npm
        JTextField npmField = new JTextField();
        npmField.setAlignmentX(Component.CENTER_ALIGNMENT);
        npmField.setFont(SistemAkademikGUI.fontGeneral);
        npmField.setMaximumSize(new Dimension(200,0));
        
        //button untuk menambahkan mahasiswa
        JButton tambahButton = new JButton("Tambahkan");
        tambahButton.setBackground(new Color(10, 196 , 196));
        tambahButton.setFont(SistemAkademikGUI.fontGeneral);
        tambahButton.setForeground(new Color(255, 255, 255));
        tambahButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        //button untuk kembali ke home
        JButton kembaliButton = new JButton("Kembali");
        kembaliButton.setBackground(new Color(10, 196 , 196));
        kembaliButton.setFont(SistemAkademikGUI.fontGeneral);
        kembaliButton.setForeground(new Color(255, 255, 255));
        kembaliButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        //actionlistener
        tambahButton.addActionListener(new ActionListener(){
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		String inputNama = namaField.getText();
        		String inputNpm = npmField.getText();
        		if (inputNama.equals("") || inputNpm.equals("")) {
        			JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
        			namaField.setText("");
        			npmField.setText("");
        		} else if (npmChecker(inputNpm, daftarMahasiswa)) {
        			long longNpm = Long.parseLong(inputNpm);
        			Mahasiswa mhs = new Mahasiswa(inputNama, longNpm);
        			daftarMahasiswa.add(mhs);
        			JOptionPane.showMessageDialog(frame, String.format("Mahasiswa %d-%s berhasil ditambahkan", mhs.getNpm(), mhs.getNama()));
        			namaField.setText("");
        			npmField.setText("");
        		} else {
        			JOptionPane.showMessageDialog(frame, String.format("NPM %s sudah pernah ditambahkan sebelumnya", inputNpm));
        			namaField.setText("");
        			npmField.setText("");
        		}
        	}
        });
        
        kembaliButton.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		frame.getContentPane().removeAll();
				frame.repaint();
				new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
		        frame.setVisible(true);
        	}
        });
        
        // menambahkan component ke panel
        panel.add(titleLabel);
        panel.add(Box.createRigidArea(new Dimension(0, 20)));
        panel.add(nama);
        panel.add(Box.createRigidArea(new Dimension(0, 20)));
        panel.add(namaField);
        panel.add(Box.createRigidArea(new Dimension(0, 20)));
        panel.add(npm);
        panel.add(Box.createRigidArea(new Dimension(0, 20)));
        panel.add(npmField);
        panel.add(Box.createRigidArea(new Dimension(0, 20)));
        panel.add(tambahButton);
        panel.add(Box.createRigidArea(new Dimension(0, 20)));
        panel.add(kembaliButton);
        panel.add(Box.createRigidArea(new Dimension(0, 20)));
        panel.add(Box.createVerticalGlue());
        
        frame.add(panel);

    }
    public boolean npmChecker(String npm, ArrayList<Mahasiswa> daftarMahasiswa) {
    	long longNPM = Long.parseLong(npm);
    	for (Mahasiswa mhs : daftarMahasiswa) {
    		if (mhs.getNpm() == longNPM) {
    			return false;
    		}
    	} return true;
    }
}
