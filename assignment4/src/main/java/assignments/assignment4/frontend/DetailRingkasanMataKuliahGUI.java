package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class DetailRingkasanMataKuliahGUI {
    public DetailRingkasanMataKuliahGUI(JFrame frame, MataKuliah mataKuliah, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // membuat panel untuk detail ringkasan matakuliah
    	JPanel detailMtkl = new JPanel();
    	detailMtkl.setLayout(new BoxLayout(detailMtkl, BoxLayout.Y_AXIS));
    	detailMtkl.setBackground(new Color(241, 186, 255));
    	detailMtkl.add(Box.createVerticalGlue());
    	JLabel titleLabel = new JLabel();
        titleLabel.setText("Detail Ringkasan Mata Kuliah");
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        detailMtkl.add(titleLabel);
        detailMtkl.add(Box.createRigidArea(new Dimension(0, 20)));
        
        // label yang diperlukan untuk detail matakuliah
        JLabel nama = new JLabel();
        nama.setText("Nama: " + mataKuliah.getNama());
        nama.setAlignmentX(Component.CENTER_ALIGNMENT);
        nama.setFont(SistemAkademikGUI.fontGeneral);
        detailMtkl.add(nama);
        detailMtkl.add(Box.createRigidArea(new Dimension(0, 20)));
        
        JLabel kode = new JLabel();
        kode.setText("NPM: " + mataKuliah.getKode());
        kode.setAlignmentX(Component.CENTER_ALIGNMENT);
        kode.setFont(SistemAkademikGUI.fontGeneral);
        detailMtkl.add(kode);
        detailMtkl.add(Box.createRigidArea(new Dimension(0, 20)));
        
        JLabel sks = new JLabel();
        sks.setText("Jurusan: " + mataKuliah.getSKS());
        sks.setAlignmentX(Component.CENTER_ALIGNMENT);
        sks.setFont(SistemAkademikGUI.fontGeneral);
        detailMtkl.add(sks);
        detailMtkl.add(Box.createRigidArea(new Dimension(0, 20)));
        
        JLabel jumlahMahasiswa = new JLabel();
        jumlahMahasiswa.setText("Jumlah mahasiswa: " + mataKuliah.getJumlahMahasiswa());
        jumlahMahasiswa.setAlignmentX(Component.CENTER_ALIGNMENT);
        jumlahMahasiswa.setFont(SistemAkademikGUI.fontGeneral);
        detailMtkl.add(jumlahMahasiswa);
        detailMtkl.add(Box.createRigidArea(new Dimension(0, 20)));
        
        JLabel kapasitas = new JLabel();
        kapasitas.setText("Kapasitas " + mataKuliah.getKapasitas());
        kapasitas.setAlignmentX(Component.CENTER_ALIGNMENT);
        kapasitas.setFont(SistemAkademikGUI.fontGeneral);
        detailMtkl.add(kapasitas);
        detailMtkl.add(Box.createRigidArea(new Dimension(0, 20)));
        
        JLabel daftarMhs = new JLabel();
        daftarMhs.setText("Daftar Mahasiswa: ");
        daftarMhs.setAlignmentX(Component.CENTER_ALIGNMENT);
        daftarMhs.setFont(SistemAkademikGUI.fontGeneral);
        detailMtkl.add(daftarMhs);
        detailMtkl.add(Box.createRigidArea(new Dimension(0, 20)));
         
        // mengiterasi daftar mahasiswa yang mengambil suatu matakuliah
        if (mataKuliah.getJumlahMahasiswa() == 0) {
        	JLabel noMhs = new JLabel();
        	noMhs.setText("Belum ada mahasiswa yang mengambil mata kuliah ini");
        	noMhs.setAlignmentX(Component.CENTER_ALIGNMENT);
        	noMhs.setFont(new Font("Century Gothic", Font.BOLD, 14));
        	detailMtkl.add(noMhs);
        	detailMtkl.add(Box.createRigidArea(new Dimension(0, 20)));
        }else {
        	for (int i = 0; i < mataKuliah.getJumlahMahasiswa(); i++) {
            	JLabel mhsAmbilMtkl = new JLabel();
                mhsAmbilMtkl.setText(mataKuliah.getDaftarMahasiswa()[i].getNama());
                mhsAmbilMtkl.setAlignmentX(Component.CENTER_ALIGNMENT);
                mhsAmbilMtkl.setFont(new Font("Century Gothic", Font.BOLD, 14));
                mhsAmbilMtkl.setAlignmentX(Component.CENTER_ALIGNMENT);
                detailMtkl.add(mhsAmbilMtkl);
                detailMtkl.add(Box.createRigidArea(new Dimension(0, 20)));
            }
        }
        
        // button untuk kembali ke home
        JButton buttonSelesai = new JButton("Selesai");
        buttonSelesai.setFont(SistemAkademikGUI.fontGeneral);
        buttonSelesai.setBackground(new Color(10, 196 , 196));
        buttonSelesai.setForeground(new Color(255, 255, 255));
        buttonSelesai.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        //action listener
        buttonSelesai.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		frame.getContentPane().removeAll();
				frame.repaint();
				new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
		        frame.setVisible(true);
        	}
        });
        detailMtkl.add(buttonSelesai);
        detailMtkl.add(Box.createRigidArea(new Dimension(0, 20)));
        detailMtkl.add(Box.createVerticalGlue());

        
        frame.add(detailMtkl);
    }
}
