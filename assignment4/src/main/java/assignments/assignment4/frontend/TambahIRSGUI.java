package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahIRSGUI {

    public TambahIRSGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // panel untuk component ke tambah irs
    	JPanel addIRS = new JPanel();
    	addIRS.setLayout(new BoxLayout(addIRS, BoxLayout.Y_AXIS));
    	addIRS.setBackground(new Color(241, 186, 255));
    	addIRS.add(Box.createVerticalGlue());
    	JLabel titleLabel = new JLabel();
        titleLabel.setText("Tambah IRS");
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        
        JLabel chooseNpm = new JLabel();
        chooseNpm.setText("Pilih NPM");
        chooseNpm.setAlignmentX(Component.CENTER_ALIGNMENT);
        chooseNpm.setFont(SistemAkademikGUI.fontGeneral);
        
        // combobox untuk memilih npm
        JComboBox npmChooser = new JComboBox(sortingNpm(daftarMahasiswa));
        npmChooser.setMaximumSize(new Dimension(200, 0));
        npmChooser.setFont(SistemAkademikGUI.fontGeneral);
        npmChooser.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        JLabel chooseMtkl = new JLabel();
        chooseMtkl.setText("Pilih Nama Matkul");
        chooseMtkl.setAlignmentX(Component.CENTER_ALIGNMENT);
        chooseMtkl.setFont(SistemAkademikGUI.fontGeneral);
        
        // combobox untuk memilih matakuliah
        JComboBox mtklChooser = new JComboBox(sortingMtkl(daftarMataKuliah));
        mtklChooser.setMaximumSize(new Dimension(200, 0));
        mtklChooser.setFont(SistemAkademikGUI.fontGeneral);
        mtklChooser.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        // button untuk menambah irs
        JButton tambahButton = new JButton("Tambahkan");
        tambahButton.setBackground(new Color(10, 196 , 196));
        tambahButton.setFont(SistemAkademikGUI.fontGeneral);
        tambahButton.setForeground(new Color(255, 255, 255));
        tambahButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        // button untuk kembali ke home
        JButton kembaliButton = new JButton("Kembali");
        kembaliButton.setBackground(new Color(10, 196 , 196));
        kembaliButton.setFont(SistemAkademikGUI.fontGeneral);
        kembaliButton.setForeground(new Color(255, 255, 255));
        kembaliButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        //action listener
        tambahButton.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		String npm = (String) npmChooser.getSelectedItem();
        		String mtkl = (String) mtklChooser.getSelectedItem();
        		if (npm == null || mtkl == null) {
        			JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
        		} else {
        			Mahasiswa mhs = getMahasiswa(Long.parseLong(npm), daftarMahasiswa);
        			MataKuliah matkul = getMataKuliah(mtkl, daftarMataKuliah);
        			JOptionPane.showMessageDialog(frame, mhs.addMatkul(matkul));
        		}
        	}
        });
        
        kembaliButton.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		frame.getContentPane().removeAll();
				frame.repaint();
				new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
		        frame.setVisible(true);
        	}
        });
        
        // menambahkan component ke dalam panel
        addIRS.add(titleLabel);
        addIRS.add(Box.createRigidArea(new Dimension(0, 20)));
        addIRS.add(chooseNpm);
        addIRS.add(Box.createRigidArea(new Dimension(0, 20)));
        addIRS.add(npmChooser);
        addIRS.add(Box.createRigidArea(new Dimension(0, 20)));
        addIRS.add(chooseMtkl);
        addIRS.add(Box.createRigidArea(new Dimension(0, 20)));
        addIRS.add(mtklChooser);
        addIRS.add(Box.createRigidArea(new Dimension(0, 20)));
        addIRS.add(tambahButton);
        addIRS.add(Box.createRigidArea(new Dimension(0, 20)));
        addIRS.add(kembaliButton);
        addIRS.add(Box.createRigidArea(new Dimension(0, 20)));
        addIRS.add(Box.createVerticalGlue());
        
        frame.add(addIRS);
    }


    
    private MataKuliah getMataKuliah(String nama, ArrayList<MataKuliah> daftarMataKuliah) {

        for (MataKuliah mataKuliah : daftarMataKuliah) {
            if (mataKuliah.getNama().equals(nama)){
                return mataKuliah;
            }
        }
        return null;
    }

    private Mahasiswa getMahasiswa(long npm, ArrayList<Mahasiswa> daftarMahasiswa) {

        for (Mahasiswa mahasiswa : daftarMahasiswa) {
            if (mahasiswa.getNpm() == npm){
                return mahasiswa;
            }
        }
        return null;
    }
    private String[] sortingMtkl(ArrayList<MataKuliah> listDaftarMataKuliah) {
    	String[] namaMatkul = new String[listDaftarMataKuliah.size()];
    	int helper = 0;
    	for (MataKuliah mtkl : listDaftarMataKuliah) {
    		namaMatkul[helper++] = mtkl.getNama();
    	}for (int i = 0; i < namaMatkul.length; i ++) {
    		for (int j = i+1; j < namaMatkul.length; j++) {
    			if (namaMatkul[i].compareTo(namaMatkul[j]) > 0) {
                    String min = namaMatkul[j];
                    namaMatkul[j] = namaMatkul[i];
                    namaMatkul[i] = min;
                }
    		}
    	}
    	return namaMatkul;
    }
    private String[] sortingNpm(ArrayList<Mahasiswa> listDaftarMahasiswa) {
    	String[] npmMahasiswa = new String[listDaftarMahasiswa.size()];
    	int helper = 0;
    	for (Mahasiswa mhs : listDaftarMahasiswa) {
    		npmMahasiswa[helper++] = Long.toString(mhs.getNpm());
    	}for (int i = 0; i < npmMahasiswa.length; i++) {
    		for (int j = i+1; j < npmMahasiswa.length; j++) {
    			if (npmMahasiswa[i].compareTo(npmMahasiswa[j]) > 0) {
                    String min = npmMahasiswa[j];
                    npmMahasiswa[j] = npmMahasiswa[i];
                    npmMahasiswa[i] = min;
                }
    		}
    	}
    	return npmMahasiswa;
    }
}
