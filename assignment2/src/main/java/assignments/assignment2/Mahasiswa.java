/*Arga Christian Roymansa
2006596554
Kelas C/MYM*/

public class Mahasiswa {
    private MataKuliah[] mataKuliah = new MataKuliah[10];
    private String[] masalahIRS;
    private int totalSKS;
    private String nama;
    private String jurusan;
    private long npm;

    public Mahasiswa(String nama, long npm){
        /* TODO: meng-set semua atribut yang dibutuhkan oleh object class Mahasiswa.*/
    	this.nama = nama;
    	this.npm = npm;
    	setJurusan(npm);
    	this.masalahIRS = new String[20];
    	setTotalSKS();
    	
    }
    /*setter getter untuk atribut yang dibutuhkan*/
    public String getNama() {
    	return this.nama;
    }
    
    public int getTotalSKS() {
    	return this.totalSKS;
    }
    
    public void setTotalSKS() {
    	/*menghitung total sks dengan mengiterasikan matakuliah yang dipelajari mahasiswa*/
    	this.totalSKS = 0;
    	for (MataKuliah matakuliah : this.mataKuliah) {
    		if (matakuliah == null) continue;
    		this.totalSKS += matakuliah.getSKS();
    	}
    	if (this.totalSKS >= 24) {
    		
    	}
    }
    public long getNPM() {
    	return this.npm;
    }
    
    public void setJurusan(long npm) {
    	/*mengeset jurusan mahasiswa sesuai dengan NPM-nya*/
    	String temp = Long.toString(npm);
    	temp = temp.substring(2,4);
    	if (temp.equals("01")) {
    		this.jurusan = "Ilmu Komputer";
    	} else {
    		this.jurusan = "Sistem Informasi";
    	}
    }
    
    public String getJurusan() {
    	return this.jurusan;
    }
    
    public MataKuliah[] getMataKuliah() {
    	return this.mataKuliah;
    }
    
    public String[] getMasalahIRS() {
    	return this.masalahIRS;
    }
    
    public void addMatkul(MataKuliah mataKuliah){
        /* TODO: menambahkan matkul ke dalam list matakuliah milik mahasiswa */
    	String masalahMatkul = String.format("Mata kuliah %s tidak dapat diambil oleh jurusan %s", mataKuliah.getNama(), this.jurusan);
    	String masalahSKS = "SKS yang anda ambil lebih dari 24.";
    	for (int i = 0; i < this.mataKuliah.length; i++) {
    		if (this.mataKuliah[i] == null) {
    			this.mataKuliah[i] = mataKuliah;
    			setTotalSKS();
    			if (this.totalSKS > 24) {
    				cekIRS(masalahSKS);
    			} if (mataKuliah.getKode().equals("IK")) {
    				if (this.jurusan.equals("Sistem Informasi")) {
    					cekIRS(masalahMatkul);
    				}
    			} else if (mataKuliah.getKode().equals("SI")) {
    				if (this.jurusan.equals("Ilmu Komputer")) {
    					cekIRS(masalahMatkul);
    				}
    			}
    			break;
    		} else if (this.mataKuliah[i] == mataKuliah) {
    			System.out.println("[DITOLAK] " + mataKuliah.getNama() + " telah diambil sebelumnya.");
    			break;
    				}
    			
    		}
    }

    public void dropMatkul(MataKuliah mataKuliah){
        /* TODO: menggunakan array baru untuk membantu mengeluarkan matakuliah yang ingin di drop dari list matakuliah milik mahasiswa.*/
    	MataKuliah[] temp = new MataKuliah[10];
    	int k = 0;
    	int nullCount = 0;
    	for (int i = 0; i < this.mataKuliah.length; i++) {
    		if (this.mataKuliah[i] == null) {
    			nullCount++;
    			continue;
    		}
    		if (this.mataKuliah[i].equals(mataKuliah)) continue;
    		temp[k++] = this.mataKuliah[i];
    	} if (k == this.mataKuliah.length - nullCount) {
    		System.out.println("[DITOLAK] " + mataKuliah.getNama() + " belum pernah diambil.");
    	}
    	for (int j = 0; j < temp.length; j++) {
    		this.mataKuliah[j] = temp[j];
    	} setTotalSKS();
    }
    
    public void urutIRS() {
    	/*mengurutkan prioritas error yang akan diprint di ringkasan mahasiswa */
    	String[] temp = new String[20];
    	int k = 0;
    	/*memprioritaskan error sks supaya diprint pertama */
    	for (int i = 0; i < this.masalahIRS.length; i++) {
    		if (this.masalahIRS[i] != null) {
    			if (this.masalahIRS[i].equals("SKS yang anda ambil lebih dari 24.")) {
    				temp[k++] = this.masalahIRS[i];
    				this.masalahIRS[i] = null;
    				break;
    			}
    		}
    	}/*memasukkan error yang lain ke dalam list error */
    	for (int i = 0; i < this.masalahIRS.length ;i++) {
    		if (this.masalahIRS[i] != null) {
    			if (temp[k++] == null) {
    				temp[k] = this.masalahIRS[i];
    			}
    		}
    	}
    	this.masalahIRS = temp;
    }
    
    public void cekIRS(String masalah){
        /* TODO: memasukkan masalah yang ada ke dalam list masalah irs mahasiswa */
    	for (int i = 0; i < this.masalahIRS.length; i++) {
    		if (this.masalahIRS[i] == null) {
    			this.masalahIRS[i] = masalah;
    			break;
    		} else if (this.masalahIRS[i].equals(masalah)) break;
    	}
    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        return this.nama;
    }

}
