/*Arga Christian Roymansa
2006596554
Kelas C/MYM*/

import java.util.Scanner;

public class SistemAkademik {
    private static final int ADD_MATKUL = 1;
    private static final int DROP_MATKUL = 2;
    private static final int RINGKASAN_MAHASISWA = 3;
    private static final int RINGKASAN_MATAKULIAH = 4;
    private static final int KELUAR = 5;
    private static Mahasiswa[] daftarMahasiswa = new Mahasiswa[100];
    private static MataKuliah[] daftarMataKuliah = new MataKuliah[100];
    
    private Scanner input = new Scanner(System.in);

    private Mahasiswa getMahasiswa(long npm) {
        /* TODO: mencari mahasiswa menggunakan npm */
    	for (Mahasiswa mahasiswa : daftarMahasiswa) {
    		if (mahasiswa == null) continue;
    		if (mahasiswa.getNPM() == npm) {
    			return mahasiswa;
    		}
    	}
        return null;
    }

    private MataKuliah getMataKuliah(String namaMataKuliah) {
        /* TODO: mencari matakuliah menggunakan matakuliah*/
    	for (MataKuliah matakuliah : daftarMataKuliah) {
    		if (matakuliah == null) continue;
    		if (matakuliah.getNama().equals(namaMataKuliah)) {
    			System.out.println(matakuliah.getNama());
    			return matakuliah;
    		}
    	}
        return null;
    }

    private void addMatkul(){
        System.out.println("\n--------------------------ADD MATKUL--------------------------\n");

        System.out.print("Masukkan NPM Mahasiswa yang akan melakukan ADD MATKUL : ");
        long npm = Long.parseLong(input.nextLine());

        /* TODO: Implementasikan kode Anda di sini 
        Jangan lupa lakukan validasi apabila banyaknya matkul yang diambil mahasiswa sudah 10*/
        Mahasiswa mahasiswa = getMahasiswa(npm);
        System.out.print("Banyaknya Matkul yang Ditambah: ");
        int banyakMatkul = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan nama matkul yang ditambah");
        for(int i=0; i<banyakMatkul; i++){
             System.out.print("Nama matakuliah " + i+1 + " : ");
             String namaMataKuliah = input.nextLine();
             /* TODO: Implementasikan kode Anda di sini */
             MataKuliah matakuliah = getMataKuliah(namaMataKuliah);
             int step = 0;
             int count = 0;
             for(MataKuliah matkul : mahasiswa.getMataKuliah()) {
                if (matkul == null) {
                	continue;
                } else {
                	count++;
                }
             } if (count + step >= 10) {
                    System.out.println("[DITOLAK] maksimal mata kuliah yang diambil hanya 10");
                    break;
                 } else if (count + step < 10) {
                    	if (matakuliah.countMahasiswa() < matakuliah.getKapasitas()) {
                    		mahasiswa.addMatkul(matakuliah);
                        	matakuliah.addMahasiswa(mahasiswa);	
                    	} else {
                    		System.out.println("[DITOLAK] "+ matakuliah.getNama() + " telah penuh kapasitasnya");
                		}
                	}
                
                
            }
        System.out.println("\nSilakan cek rekap untuk melihat hasil pengecekan IRS.\n");
        
    }

    private void dropMatkul(){
        System.out.println("\n--------------------------DROP MATKUL--------------------------\n");

        System.out.print("Masukkan NPM Mahasiswa yang akan melakukan DROP MATKUL : ");
        long npm = Long.parseLong(input.nextLine());
        Mahasiswa mahasiswa = getMahasiswa(npm);

       /* TODO: Implementasikan kode Anda di sini 
        Jangan lupa lakukan validasi apabila mahasiswa belum mengambil mata kuliah*/
        int nullCount = 0;
        for (MataKuliah matakuliah : mahasiswa.getMataKuliah()) {
        	if (matakuliah == null) {
        		nullCount++;
        	}
        } 
        if (nullCount == mahasiswa.getMataKuliah().length) {
        	System.out.println("Belum ada mata kuliah yang diambil");
        } else {
        	System.out.print("Banyaknya Matkul yang Di-drop: ");
        	int banyakMatkul = Integer.parseInt(input.nextLine());
            System.out.println("Masukkan nama matkul yang di-drop:");
            for(int i=0; i<banyakMatkul; i++){
                System.out.print("Nama matakuliah " + i+1 + " : ");
                String namaMataKuliah = input.nextLine();
                /* TODO: Implementasikan kode Anda di sini */
                MataKuliah matkul = getMataKuliah(namaMataKuliah);
                for (int j = 0; j < mahasiswa.getMataKuliah().length; j++) {
                	if (mahasiswa.getMataKuliah()[j] == null) continue;
                		mahasiswa.dropMatkul(matkul);
                		matkul.dropMahasiswa(mahasiswa);
                		break;
                } 
            }
        }
        
        System.out.println("\nSilakan cek rekap untuk melihat hasil pengecekan IRS.\n");
    }

    private void ringkasanMahasiswa(){
        System.out.print("Masukkan npm mahasiswa yang akan ditunjukkan ringkasannya : ");
        long npm = Long.parseLong(input.nextLine());
        Mahasiswa mahasiswa = getMahasiswa(npm);
        // TODO: Isi sesuai format keluaran
        System.out.println("\n--------------------------RINGKASAN--------------------------\n");
        System.out.println("Nama: " + mahasiswa.getNama());
        System.out.println("NPM: " + npm);
        System.out.println("Jurusan: " + mahasiswa.getJurusan());
        System.out.println("Daftar Mata Kuliah: ");
        /* TODO: Cetak daftar mata kuliah 
        Handle kasus jika belum ada mata kuliah yang diambil*/
        int k = 1;
        for (MataKuliah matkul : mahasiswa.getMataKuliah()) {
        	if (matkul == null) continue;
        	System.out.println(Integer.toString(k++) + ". " + matkul.getNama());
        }

        System.out.println("Total SKS: " + mahasiswa.getTotalSKS());
        /* TODO: Cetak hasil cek IRS
        Handle kasus jika IRS tidak bermasalah */
        
        System.out.println("Hasil Pengecekan IRS:");
        mahasiswa.urutIRS();
        int j = 0;
        for (int i = 0; i < mahasiswa.getMasalahIRS().length; i++) {
        	if (mahasiswa.getMasalahIRS()[i] == null) continue;
        	j += 1;
        } if (j == 0) {
        	System.out.println("IRS tidak bermasalah");
        } else {
        	int helper = 1;
        	for (String masalah : mahasiswa.getMasalahIRS()) {
        		if (masalah == null) continue;
        		else {
        			System.out.println(helper++ +". " + masalah);
        		}
        	}
        }
        
    }

    private void ringkasanMataKuliah(){
        System.out.print("Masukkan nama mata kuliah yang akan ditunjukkan ringkasannya : ");
        String namaMataKuliah = input.nextLine();
        MataKuliah matkul = getMataKuliah(namaMataKuliah);
        
        // TODO: Isi sesuai format keluaran
        System.out.println("\n--------------------------RINGKASAN--------------------------\n");
        System.out.println("Nama mata kuliah: " + matkul.getNama());
        System.out.println("Kode: " + matkul.getKode());
        System.out.println("SKS: " + matkul.getSKS());
        System.out.println("Jumlah mahasiswa: " + matkul.countMahasiswa());
        System.out.println("Kapasitas: " + matkul.getKapasitas());
        System.out.println("Daftar mahasiswa yang mengambil mata kuliah ini: ");
       /* TODO: Cetak hasil cek IRS
        Handle kasus jika tidak ada mahasiswa yang mengambil */
        int k = 1;
        int nullCount = 0;
        for (Mahasiswa mahasiswa : matkul.getDaftarMahasiswa()) {
            if (mahasiswa == null) {
                nullCount++;
            	continue;
            } else {
            	System.out.println(k++ + ". " + mahasiswa.getNama());	
            }
       } if (nullCount == matkul.getDaftarMahasiswa().length) {
    	   System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini.");	
            }
        }


    private void daftarMenu(){
        int pilihan = 0;
        boolean exit = false;
        while (!exit) {
            System.out.println("\n----------------------------MENU------------------------------\n");
            System.out.println("Silakan pilih menu:");
            System.out.println("1. Add Matkul");
            System.out.println("2. Drop Matkul");
            System.out.println("3. Ringkasan Mahasiswa");
            System.out.println("4. Ringkasan Mata Kuliah");
            System.out.println("5. Keluar");
            System.out.print("\nPilih: ");
            try {
                pilihan = Integer.parseInt(input.nextLine());
            } catch (NumberFormatException e) {
                continue;
            }
            System.out.println();
            if (pilihan == ADD_MATKUL) {
                addMatkul();
            } else if (pilihan == DROP_MATKUL) {
                dropMatkul();
            } else if (pilihan == RINGKASAN_MAHASISWA) {
                ringkasanMahasiswa();
            } else if (pilihan == RINGKASAN_MATAKULIAH) {
                ringkasanMataKuliah();
            } else if (pilihan == KELUAR) {
                System.out.println("Sampai jumpa!");
                exit = true;
            }
        }

    }


    private void run() {
        System.out.println("====================== Sistem Akademik =======================\n");
        System.out.println("Selamat datang di Sistem Akademik Fasilkom!");
        
        System.out.print("Banyaknya Matkul di Fasilkom: ");
        int banyakMatkul = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan matkul yang ditambah");
        System.out.println("format: [Kode Matkul] [Nama Matkul] [SKS] [Kapasitas]");

        for(int i=0; i<banyakMatkul; i++){
            String[] dataMatkul = input.nextLine().split(" ", 4);
            int sks = Integer.parseInt(dataMatkul[2]);
            int kapasitas = Integer.parseInt(dataMatkul[3]);
            /* TODO: Buat instance mata kuliah dan masukkan ke dalam Array */
            MataKuliah matkul = new MataKuliah(dataMatkul[0], dataMatkul[1], sks, kapasitas);
            for (int j = 0; j < daftarMataKuliah.length; j++) {
            	if (daftarMataKuliah[j] == null) {
            		daftarMataKuliah[j] = matkul;
            		break;
            	}
            }
        }

        System.out.print("Banyaknya Mahasiswa di Fasilkom: ");
        int banyakMahasiswa = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan data mahasiswa");
        System.out.println("format: [Nama] [NPM]");

        for(int i=0; i<banyakMahasiswa; i++){
            String[] dataMahasiswa = input.nextLine().split(" ", 2);
            long npm = Long.parseLong(dataMahasiswa[1]);
            /* TODO: Buat instance mata kuliah dan masukkan ke dalam Array */
            Mahasiswa mahasiswa = new Mahasiswa(dataMahasiswa[0], npm);
            for (int j = 0; j < daftarMahasiswa.length; j++) {
            	if (daftarMahasiswa[j] == null) {
            		daftarMahasiswa[j] = mahasiswa;
            		break;
            	}
            }
        }

        daftarMenu();
        input.close();
    }


	public static void main(String[] args) {
        SistemAkademik program = new SistemAkademik();
        program.run();
    }


    
}
