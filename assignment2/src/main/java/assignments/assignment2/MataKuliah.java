/*Arga Christian Roymansa
2006596554
Kelas C/MYM*/

public class MataKuliah {
    private String kode;
    private String nama;
    private int sks;
    private int kapasitas;
    private Mahasiswa[] daftarMahasiswa;

    public MataKuliah(String kode, String nama, int sks, int kapasitas){
    	 /* TODO: meng-set semua atribut yang dibutuhkan oleh object class matakuliah.*/
    	this.kode = kode;
    	this.nama = nama;
    	this.sks = sks;
    	this.kapasitas = kapasitas;
    	this.daftarMahasiswa = new Mahasiswa[10];
    }
    /*setter getter untuk atribut yang dimiliki matakuliah*/
    public String getKode() {
    	return this.kode;
    }
    
    public String getNama() {
    	return this.nama;
    }
    
    public int getSKS() {
    	return this.sks;
    }
    
    public int getKapasitas() {
    	return this.kapasitas;
    }
    
    public Mahasiswa[] getDaftarMahasiswa() {
    	return this.daftarMahasiswa;
    }
    
    public int countMahasiswa() {
    	/*menghitung jumlah mahasiswa yang mengambil matakuliah tsb*/
    	int jumlahMahasiswa = 0;
    	if (this.daftarMahasiswa == null) {
    		return jumlahMahasiswa;
    	} else {
    		for (Mahasiswa mahasiswa : this.daftarMahasiswa) {
    			if (mahasiswa == null) continue;
        		jumlahMahasiswa++;
        		} return jumlahMahasiswa;
    		}
    }
    
    public void addMahasiswa(Mahasiswa mahasiswa) {
        /* TODO: menambahkan mahasiswa ke dalam list orang yang mengambil matakuliah tsb */
    	for (int i = 0; i < this.daftarMahasiswa.length; i++) {
    		if (this.daftarMahasiswa[i] == null) {
    			this.daftarMahasiswa[i] = mahasiswa;
    			break;
    		}
    	}
    }

    public void dropMahasiswa(Mahasiswa mahasiswa) {
        /* TODO: menggunakan new array untuk membantu mengeluarkan orang yang diinginkan dari list matakuliah*/
    	Mahasiswa[] temp = new Mahasiswa[10];
    	int k = 0;
    	for (int i = 0; i < this.daftarMahasiswa.length; i++) {
    		if (this.daftarMahasiswa[i] == mahasiswa) continue;
    		temp[k++] = this.daftarMahasiswa[i];
    	} for (int j = 0; j < temp.length; j++) {
    		this.daftarMahasiswa[j] = temp[j];
    	} 
    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        return "";
    }
}
