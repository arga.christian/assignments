class Mahasiswa extends ElemenFasilkom {

    private MataKuliah[] daftarMataKuliah = new MataKuliah[10];
    
    private long npm;

    private String tanggalLahir;
    
    private String jurusan;

    public Mahasiswa(String nama, long npm) {
        /* constructor untuk elemen mahasiswa */
    	super(nama);
    	this.tipe = "Mahasiswa";
    	this.npm = npm;
    	this.setTanggalLahir();
    	this.setJurusan();
    }
    
    /* setter dan getter yang diperlukan untuk mahasiswa */
    
    public void setTanggalLahir() {
    	this.tanggalLahir = this.extractTanggalLahir(npm);
    }
    
    public String getTanggalLahir() {
    	return this.tanggalLahir;
    }
    
    public void setJurusan() {
    	this.jurusan = this.extractJurusan(npm);
    }
    
    public String getJurusan() {
    	return this.jurusan;
    }
    
    public MataKuliah[] getDaftarMataKuliah() {
    	return this.daftarMataKuliah;
    }

    public void addMatkul(MataKuliah mataKuliah) {
        /* method untuk menambahkan matkul, matakuliah akan dimasukkan ke dalam list */
    	for (int i = 0; i < this.daftarMataKuliah.length; i++) {
    		/* jika nilai elemen dalam list masih null, maka matakuliah akan ditambahkan ke dalam list */
    		if (this.daftarMataKuliah[i] == null) {
    			this.daftarMataKuliah[i] = mataKuliah;
    			System.out.printf("%s berhasil menambahkan mata kuliah %s\n", this.nama, mataKuliah);
    			break;
    		} else if (this.daftarMataKuliah[i].getNama().equals(mataKuliah.getNama())) {
    			System.out.printf("[DITOLAK] %s telah diambil sebelumnya\n", mataKuliah);
    			break;
    		}
    	} 
    }

    public void dropMatkul(MataKuliah mataKuliah) {
        /* method untuk meng-drop matkul, matakuliah akan didrop dari list 
         * jika nilai object pertama di list masih null, artinya mahasiswa belum mengambil matkul */
    	if (daftarMataKuliah[0] == null) {
    		System.out.printf("[DITOLAK] %s belum pernah diambil\n", mataKuliah);
    	}
    	/*jika nilainya tidak null, nilai tersebut akan dijadikan null */
    	else {
    		for (int i = 0; i < this.daftarMataKuliah.length; i++) {
    			if (this.daftarMataKuliah[i] == null) continue;
    			else if (this.daftarMataKuliah[i].getNama().equals(mataKuliah.getNama())) {
    				this.daftarMataKuliah[i] = null;
    				System.out.printf("%s berhasil drop mata kuliah %s\n", this.nama, mataKuliah);
    				break;
    			} 
    			/* jika nilai matakuliah tidak sama, akan dicek apakah sudah mengiterasi semua object dilist */
    			else {
    				/* jika belum. iterasi akan dilanjutkan */
    				if (i < this.daftarMataKuliah.length) {
    					continue;
    				} 
    				/* jika sudah, maka matakuliah tersebut belum pernah diambil oleh mahasiswa */
    				else {
    					System.out.printf("[DITOLAK] %s belum pernah diambil\n", mataKuliah);
    				}
    			}
        	}
    	}
    	
    }

    public String extractTanggalLahir(long npm) {
        /* method untuk mencari tanggal lahir mahasiswa */
    	String newNpm = Long.toString(npm);
        String tanggalLahir = newNpm.substring(4, 6);
        String bulanLahir = newNpm.substring(6, 8);
        String tahunLahir = newNpm.substring(8, 12);
        return String.format("%s-%s-%s", tanggalLahir, bulanLahir,tahunLahir);
    }

    public String extractJurusan(long npm) {
        /* method untuk menentukan jurusan mahasiswa */
        String newNpm = Long.toString(npm);
        if (newNpm.substring(2,4).equals("01")) {
        	return "Ilmu Komputer";
        } else {
        	return "Sistem Informasi";
        }
    }
}