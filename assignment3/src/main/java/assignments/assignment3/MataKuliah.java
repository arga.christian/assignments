class MataKuliah {

    private String nama;
    
    private int kapasitas;

    private Dosen dosen;

    private Mahasiswa[] daftarMahasiswa;
    
    private int jumlahSiswa;

    public MataKuliah(String nama, int kapasitas) {
        /* Constructor untuk matakuliah */
    	this.nama = nama;
    	this.kapasitas = kapasitas;
    	this.dosen = null;
    	this.daftarMahasiswa = new Mahasiswa[kapasitas];
    }
    
    /* setter dan getter yang diperlukan untuk matakuliah */
    
	public Dosen getDosen() {
		return this.dosen;
	}
	
	public String getNama() {
		return this.nama;
	}
	
	public int getJumlahMahasiswa() {
		return this.jumlahSiswa;
	}
    
	public int getKapasitas() {
		return this.kapasitas;
	}
	
	public Mahasiswa[] getDaftarMahasiswa() {
		return this.daftarMahasiswa;
	}
	
    public void addMahasiswa(Mahasiswa mahasiswa) {
        /* method untuk menambahkan mahasiswa ke dalam list */
    	for (int i = 0; i < this.daftarMahasiswa.length; i++) {
    		if (this.daftarMahasiswa[i] == null) {
    			this.daftarMahasiswa[i]= mahasiswa;
    			break;
    		} 
    	}
    	/* jika sudah ditambahkan, nilai jumlahSiswa juga akan ditambah */
    	this.jumlahSiswa++;
    }

    public void dropMahasiswa(Mahasiswa mahasiswa) {
        /* method untuk mengurangi nilai mahasiswa dari list */
    	Mahasiswa[] temp = new Mahasiswa[this.kapasitas];
    	int helper = 0;
    	for (int i = 0; i < this.daftarMahasiswa.length; i++) {
    		if (daftarMahasiswa[i] == null) break;
    		else if (this.daftarMahasiswa[i].equals(mahasiswa)) continue;
			else {
    			this.daftarMahasiswa[i] = temp[helper++];
    		}
    	}
    	daftarMahasiswa = temp;
    	/* jika sudah dikeluarkan dari list, nilai jumlahsiswa juga akan dikurangi */
    	this.jumlahSiswa--;
    }

    public void addDosen(Dosen dosen) {
        /* method menambahkan dosen pengajar */
    	if (dosen.getMataKuliah().getNama() == this.nama) {
    		this.dosen = dosen;
    	} 
    }

    public void dropDosen() {
        /* method untuk drop dosen pengajar */
    	this.dosen = null;
    }

    public String toString() {
        return this.nama;
    }


}