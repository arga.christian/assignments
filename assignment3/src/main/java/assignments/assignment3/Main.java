import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    private static ElemenFasilkom[] daftarElemenFasilkom = new ElemenFasilkom[100];

    private static MataKuliah[] daftarMataKuliah = new MataKuliah[100];

    private int totalMataKuliah = 0;

    private static int totalElemenFasilkom = 0; 
    


    public static void addMahasiswa(String nama, long npm) {
        /* Membuat object mahasiswa dan dimasukkan ke dalam list */
    	Mahasiswa mhs = new Mahasiswa(nama, npm);
    	for(int i = 0; i < daftarElemenFasilkom.length; i++) {
    		if (daftarElemenFasilkom[i] == null) {
    			daftarElemenFasilkom[i] = mhs;
    			totalElemenFasilkom++;
    			System.out.printf("%s berhasil ditambahkan\n", mhs);
    			break;
    		}
    	}
    }

    public static void addDosen(String nama) {
        /* Membuat object dosen dan dimasukkan ke dalam list */
    	Dosen dsn = new Dosen(nama);
    	for(int i = 0; i < daftarElemenFasilkom.length; i++) {
    		if (daftarElemenFasilkom[i] == null) {
    			daftarElemenFasilkom[i] = dsn;
    			totalElemenFasilkom++;
    			System.out.printf("%s berhasil ditambahkan\n", dsn);
    			break;
    		}
    	}
    }

    static void addElemenKantin(String nama) {
        /* Membuat object elemen kantin dan dimasukkan ke dalam list */
    	ElemenKantin kantin = new ElemenKantin(nama);
    	for(int i = 0; i < daftarElemenFasilkom.length; i++) {
    		if (daftarElemenFasilkom[i] == null) {
    			daftarElemenFasilkom[i] = kantin;
    			totalElemenFasilkom++;
    			System.out.printf("%s berhasil ditambahkan\n", kantin);
    			break;
    		}
    	}
    }

    static void menyapa(String objek1, String objek2) {
        /* Method menyapa, mencari elemen dengan nama objek1 dan objek2 */
    	ElemenFasilkom newObject1 = null;
    	ElemenFasilkom newObject2 = null;
    	for (ElemenFasilkom elemen : daftarElemenFasilkom) {
    		if (elemen == null) continue;
    		else if (elemen.getNama().equals(objek1)) {
    			newObject1 = elemen;
    		} else if (elemen.getNama().equals(objek2)) {
    			newObject2 = elemen;
    		}
    	} /* Jika objek1 dan objek 2 sama, maka akan dikeluarkan output seperti di bawah */
    	if (objek1.equals(objek2)) {
    		System.out.printf("[DITOLAK] Objek yang sama tidak bisa saling menyapa\n");	
    	}/* Mengecek apakah yang menyapa adalah dosen atau mahasiswa */
    	else if (newObject1.getTipe().equals("Dosen") && newObject2.getTipe().equals("Mahasiswa")) {
    		Dosen dsn = (Dosen) newObject1;
    		Mahasiswa mhs = (Mahasiswa) newObject2;
    		/* Untuk setiap matakuliah yang dimiliki mahasiswa, jika diajar oleh dosen yang disapa,
    		 * friendship dari kedua elemen akan dijumlahkan */
    		for (int i = 0; i < newObject1.getTelahMenyapa().length; i++) {
    			if (newObject1.getTelahMenyapa()[i] == null) {
    				System.out.printf("%s menyapa dengan %s\n", objek1, objek2);
    	    		for (MataKuliah matkul : mhs.getDaftarMataKuliah()) {
    	    			dsn.menyapa(mhs);
    					mhs.menyapa(dsn);
    	    			if (matkul == null) continue;
    	    			else if (matkul.equals(dsn.getMataKuliah())) {
    	    				dsn.tambahFriendship(2);
    	    				mhs.tambahFriendship(2);
    	    			} 
    	    		} break;
    			} /* Jika sudah pernah disapa hari ini, maka akan dikeluarkan output seperti di bawah */ 
    			else if (newObject1.getTelahMenyapa()[i].getNama().equals(newObject2.getNama())) {
    				System.out.printf("[DITOLAK] %s telah menyapa %s hari ini\n", objek1, objek2);
    			}
    		}
    		
    	} /* antisipasi apabila objek1 yang mahasiswa dan objek2 yang dosen */
    	else if (newObject1.getTipe().equals("Mahasiswa") && newObject2.getTipe().equals("Dosen")) {
    		Dosen dsn = (Dosen) newObject2;
    		Mahasiswa mhs = (Mahasiswa) newObject1;
    		for (int i = 0; i < newObject1.getTelahMenyapa().length; i++) {
    			if (newObject1.getTelahMenyapa()[i] == null) {
    				System.out.printf("%s menyapa dengan %s\n", objek1, objek2);
    	    		for (MataKuliah matkul : mhs.getDaftarMataKuliah()) {
    	    			dsn.menyapa(mhs);
    					mhs.menyapa(dsn);
    	    			if (matkul == null) continue;
    	    			else if (matkul.equals(dsn.getMataKuliah())) {
    	    				dsn.tambahFriendship(2);
    	    				mhs.tambahFriendship(2);
    	    			} 
    	    		} break;
    			} else if (newObject1.getTelahMenyapa()[i].getNama().equals(newObject2.getNama())) {
    				System.out.printf("[DITOLAK] %s telah menyapa %s hari ini\n", objek1, objek2);
    			}
    		}
    	}/* jika bukan interaksi antar mahasiswa dan dosen, masih tetap menyapa tapi tidak ada penambahan poin */
    	else {
    		for (ElemenFasilkom elm : newObject1.telahMenyapa) {
    			if (elm == null) {
        				newObject1.menyapa(newObject2);
        				newObject2.menyapa(newObject1);
        				System.out.printf("%s menyapa dengan %s\n", objek1, objek2);
        				break;
        		} else if (elm.getNama().equals(objek2)) {
    				System.out.printf("[DITOLAK] %s telah menyapa %s hari ini\n", objek1, objek2);
    				break;
    			}
				
    		}
    	}  
    }

    static void addMakanan(String objek, String namaMakanan, long harga) {
        /* method menambah makanan, akan membuat objek makanan dan baru dan dimasukkan ke dalam list makanan objek */
    	ElemenFasilkom newObject1 = null;
    	for (ElemenFasilkom elemen : daftarElemenFasilkom) {
    		if (elemen.getNama().equals(objek)) {
    			newObject1 = elemen;
    			if (newObject1.getTipe().equals("Elemen Kantin")) {
    	    		ElemenKantin object1 = (ElemenKantin) newObject1;
    	    		object1.setMakanan(namaMakanan, harga);
    	    		break;
    	    		/* jika objek bukan elemen kantin, akan dikeluarkan output seperti di bawah */
    	    	} else {
    	    		System.out.printf("[DITOLAK] %s bukan merupakan elemen kantin\n", newObject1.getNama());
    	    		break;
    	    	}
    		}
    	} 
    }

    static void membeliMakanan(String objek1, String objek2, String namaMakanan) {
        /* method untuk membeli makanan, mencari objek1 dan objek2 dari daftarElemenFasilkom */
    	ElemenFasilkom newObject1 = null;
    	ElemenFasilkom newObject2 = null;
    	for (ElemenFasilkom elemen : daftarElemenFasilkom) {
    		if (elemen == null) continue;
    		else if (elemen.getNama().equals(objek1)) {
    			newObject1 = elemen;
    		} else if (elemen.getNama().equals(objek2)) {
    			newObject2 = elemen;
    		}
    	}/* jika kedua objek sama dan merupakan elemen kantin, maka akan dikeluarkan output seperti di bawah */ 
    	if (objek1.equals(objek2) && newObject1.getTipe().equals("Elemen Kantin")) {
    		System.out.printf("[DITOLAK] Elemen kantin tidak bisa membeli makanan sendiri\n");
    	} /* jika objek kedua (penjual) bukan elemen kantin, akan dikeluarkan output seperti di bawah */ 
    	else if (!newObject2.getTipe().equals("Elemen Kantin")) {
    		System.out.printf("[DITOLAK] Hanya elemen kantin yang dapat menjual makanan\n");
    	}  else {
    		newObject1.membeliMakanan(newObject1, newObject2, namaMakanan);

    	}
    }

    static void createMatkul(String nama, int kapasitas) {
        /* method untuk membuat matkul, matkul akan dimasukkan ke dalam list daftarMataKuliah */
    	MataKuliah matkul = new MataKuliah(nama, kapasitas);
    	for (int i = 0; i < daftarMataKuliah.length; i++) {
    		if (daftarMataKuliah[i] == null) {
    			daftarMataKuliah[i] = matkul;
    			System.out.printf("%s berhasil ditambahkan dengan kapasitas %d\n", matkul, matkul.getKapasitas());
    			break;
    		}
    	}
    }

    static void addMatkul(String objek, String namaMataKuliah) {
        /* Method untuk menambahkan matkul ke list matkul milik objek */
    	ElemenFasilkom newObjek = null;
    	for (ElemenFasilkom elemen : daftarElemenFasilkom) {
    		if (elemen.getNama().equals(objek)) {
    			newObjek = elemen;
    			break;
    		}
    	}
    	MataKuliah newMatkul = null;
    	for (MataKuliah matkul : daftarMataKuliah) {
    		if (matkul.getNama().equals(namaMataKuliah)) {
    			newMatkul = matkul;
    			break;
    		}
    	} /* jika objek adalah mahasiswa, maka nama mahasiswa akan ditambahkan ke dalam list mahasiswa milik matkul*/ 
    	if (newObjek.getTipe().equals("Mahasiswa")) {
    		Mahasiswa mahasiswa = (Mahasiswa) newObjek;
    		newMatkul.addMahasiswa(mahasiswa);
    		/* jika sesuai dengan kapasitas maka matkul akan ditambahkan ke list matkul milik mahasiswa */
    		if (newMatkul.getKapasitas() > newMatkul.getJumlahMahasiswa() - 1) {
    			mahasiswa.addMatkul(newMatkul);
    		} /* jika tidak maka matkul tidak ditambahkan */ 
    		else {
    			System.out.printf("[DITOLAK] %s telah penuh kapasitasnya.\n",  newMatkul);
    		}
    	} /* jika objek bukan mahasiswa, akan dikeluarkan output seperti di bawah */ 
    	else {
    		System.out.printf("[DITOLAK] Hanya mahasiswa yang dapat menambahkan matkul\n");
    	}
    }

    static void dropMatkul(String objek, String namaMataKuliah) {
        /* method untuk dropmatkul, matkul akan dikeluarkan dari list matkul milik mahasiswa */
    	ElemenFasilkom newObjek = null;
    	for (ElemenFasilkom elemen : daftarElemenFasilkom) {
    		if (elemen.getNama().equals(objek)) {
    			newObjek = elemen;
    			break;
    		}
    	}
    	MataKuliah newMatkul = null;
    	for (MataKuliah matkul : daftarMataKuliah) {
    		if (matkul.getNama().equals(namaMataKuliah)) {
    			newMatkul = matkul;
    			break;
    		}
    	} if (newObjek.getTipe().equals("Mahasiswa")) {
    		Mahasiswa mahasiswa = (Mahasiswa) newObjek;
    		mahasiswa.dropMatkul(newMatkul);
    		newMatkul.dropMahasiswa(mahasiswa);
    	} else {
    		System.out.printf("[DITOLAK] Hanya mahasiswa yang dapat drop matkul\n");
    	}
    }

    static void mengajarMatkul(String objek, String namaMataKuliah) {
        /* method untuk meng-assign dosen ke satu mata pelajaran */
    	ElemenFasilkom newObjek = null;
    	for (ElemenFasilkom elemen : daftarElemenFasilkom) {
    		if (elemen.getNama().equals(objek)) {
    			newObjek = elemen;
    			break;
    		}
    	}
    	MataKuliah newMatkul = null;
    	for (MataKuliah matkul : daftarMataKuliah) {
    		if (matkul== null) break;
    		else if (matkul.getNama().equals(namaMataKuliah)) {
    			newMatkul = matkul;
    			break;
    		}
    	} if (newObjek.getTipe().equals("Dosen")) {
    		Dosen dsn = (Dosen) newObjek;
    		dsn.mengajarMataKuliah(newMatkul);
    		newMatkul.addDosen(dsn);
    	} else {
    		System.out.printf("[DITOLAK] Hanya dosen yang dapat mengajar matkul\n");
    	}
    }

    static void berhentiMengajar(String objek) {
        /* method untuk meng-drop dosen dari satu matakuliah, dan matakuliah dari satu dosen */
    	ElemenFasilkom newObjek = null;
    	for (ElemenFasilkom elemen : daftarElemenFasilkom) {
    		if (elemen.getNama().equals(objek)) {
    			newObjek = elemen;
    			break;
    		}
    	} if (newObjek.getTipe().equals("Dosen")) {
    		Dosen dsn = (Dosen) newObjek;
    		if (dsn.getMataKuliah() == null) {
    			dsn.dropMataKuliah();
    		} else {
    			dsn.getMataKuliah().dropDosen();
    			dsn.dropMataKuliah();
    		}
    	} else {
    		System.out.printf("[DITOLAK] Hanya dosen yang dapat berhenti mengajar\n");
    	}
    }

    static void ringkasanMahasiswa(String objek) {
        /* menghasilkan output ringkasan tentang satu mahasiswa */
    	ElemenFasilkom newObjek = null;
    	for (ElemenFasilkom elemen : daftarElemenFasilkom) {
    		if (elemen.getNama().equals(objek)) {
    			newObjek = elemen;
    			break;
    		}
    	} if (newObjek.getTipe().equals("Mahasiswa")) {										//mengecek apakah objek berupa mahasiswa
    		Mahasiswa mahasiswa = (Mahasiswa) newObjek;										//mengcasting elemenFasilkom newObjek menjadi class Mahasiswa
    		System.out.printf("nama: %s\n", mahasiswa);										//mengeprint nama mahasiswa
    		System.out.printf("tanggal lahir: %s\n", mahasiswa.getTanggalLahir());			//mengeprint tanggal lahir mahasiswa
    		System.out.printf("jurusan: %s\n", mahasiswa.getJurusan());						//mengeprint jurusan mahasiswa
    		int helper = 0;																	//int helper digunakan sebagai awalan dalam mengeprint list matakuliah
    		System.out.println("Daftar mata kuliah: ");
    		if (mahasiswa.getDaftarMataKuliah()[0] == null) {								//jika list awal masih null, dapat disimpulkan tidak ada mata kuliah yang diambil
    			System.out.println("Belum ada mata kuliah yang diambil");
    		}
    		else {
    			for (MataKuliah matakuliah : mahasiswa.getDaftarMataKuliah()) {				//mengiterasi tiap matakuliah dari list matakuliah milik mahasiswa
    				if (!(matakuliah == null)) {
    					System.out.printf("%d. %s\n", ++helper, matakuliah);				//akan diprint jika nilainya tidak null
    				}
    			}
    		}
    	} else {
    		System.out.printf("[DITOLAK] %s bukan merupakan seorang mahasiswa\n", newObjek.getNama());	//tidak akan mengeluarkan ringkasan karena bukan mahasiswa
    	}
    }

    static void ringkasanMataKuliah(String namaMataKuliah) {
        /* menghasilkan output ringkasan tentang satu matakuliah */
    	MataKuliah newMatkul = null;
    	for (MataKuliah matkul : daftarMataKuliah) {
    		if (matkul.getNama().equals(namaMataKuliah)) {
    			newMatkul = matkul;
    			break;
    		}
    	}
    	System.out.printf("Nama mata kuliah: %s\n", newMatkul);								//mengeprint nama matakuliah
    	System.out.printf("Jumlah mahasiswa: %d\n", newMatkul.getJumlahMahasiswa());		//mengeprint jumlah mahasiswa
    	System.out.printf("Kapasitas: %d\n", newMatkul.getKapasitas());						//mengeprint kapasitas
    	if (newMatkul.getDosen() == null) {
    		System.out.printf("Dosen pengajar: Belum ada\n");								//mengeprint belum ada dosen pengajar jika masih null
    	} else {
    		System.out.printf("Dosen: %s\n",newMatkul.getDosen());
    	}
    	int helper = 0;																		//int helper digunakan sebagai awalan dalam mengeprint list mahasiswa
    	System.out.println("Daftar mahasiswa yang mengambil matakuliah: ");
    	if (newMatkul.getDaftarMahasiswa()[0] == null) {									//jika list awal masih null, dapat disimpulkan tidak ada mahasiswa yang mengambil
    		System.out.println("Belum ada mahasiswa yang mengambil mata kuliah");
    	} else {
    		for (Mahasiswa mhs : newMatkul.getDaftarMahasiswa()) {
    			if (!(mhs == null)) {														//jika nilainya bukan null akan diprint
    				System.out.printf("%d. %s\n", ++helper, mhs);
    			}
    		}
    	}
    }

    static void nextDay() {
        /* menghitung jumlah elemen yang telah disapa dan menambah/mengurangi friendship point */
    	for (ElemenFasilkom elemen : daftarElemenFasilkom) {
    		if (elemen == null) break;
    		else if (elemen.getJumlahDisapa() >= (totalElemenFasilkom-1)/2) {
    			elemen.tambahFriendship(10);
    		} else {
    			elemen.kurangFriendship(5);
    		}
    	} System.out.println("Hari telah berakhir dan nilai friendship telah diupdate"); 
    	friendshipRanking();
    }

    static void friendshipRanking() {
        /* mengurutkan ranking friendship dari yang paling tinggi ke paling rendah */
    	for (int i = 0; i < daftarElemenFasilkom.length; i++) {
    		for (int j = i+1; j < daftarElemenFasilkom.length; j++) {
    			if (daftarElemenFasilkom[j] == null) break;
        		else if (daftarElemenFasilkom[i].getFriendship() == daftarElemenFasilkom[j].getFriendship()) {
        			for (int k = 0; k < Math.min(daftarElemenFasilkom[i].getNama().length(), daftarElemenFasilkom[j].getNama().length()); k++) {
        				if (daftarElemenFasilkom[i].getNama().charAt(k) < daftarElemenFasilkom[j].getNama().charAt(k)) break;
        				else if (daftarElemenFasilkom[i].getNama().charAt(k) > daftarElemenFasilkom[j].getNama().charAt(k)) {
        					ElemenFasilkom currentElement = daftarElemenFasilkom[i];
        					ElemenFasilkom nextElement = daftarElemenFasilkom[j];
        					daftarElemenFasilkom[i] = nextElement;
        					daftarElemenFasilkom[j] = currentElement;
        					break;
        				}
        			}
        		}/* jika nilai point elemen indeks lebih kecil lebih kecil dari indeks yang lebih besar, maka urutannya akan diubah */ 
        		else if (daftarElemenFasilkom[i].getFriendship() < daftarElemenFasilkom[j].getFriendship()) {
        			ElemenFasilkom currentElement = daftarElemenFasilkom[i];
        			ElemenFasilkom nextElement = daftarElemenFasilkom[j];
        			daftarElemenFasilkom[i] = nextElement;
        			daftarElemenFasilkom[j] = currentElement;
        		}
    		}
    	} int helper = 0; 
    	/* mengeprint tiap iterasi yang sudah diurutkan */
    	for (ElemenFasilkom elemen : daftarElemenFasilkom) {
    		if (elemen == null) break;
    		elemen.resetMenyapa();
    		System.out.printf("%d. %s (%d)\n", ++helper, elemen.getNama(), elemen.getFriendship());
    	}
    } 	

    static void programEnd() {
        /* akhir dari program, akan mengeprint hasil akhir ranking friendship */
    	System.out.println("Program telah berakhir. Berikut nilai terakhir dari friendship pada fasilkom");
    	friendshipRanking();
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
      
        while (true) {
            String in = input.nextLine();
            if (in.split(" ")[0].equals("ADD_MAHASISWA")) {
                addMahasiswa(in.split(" ")[1], Long.parseLong(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_DOSEN")) {
                addDosen(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("ADD_ELEMEN_KANTIN")) {
                addElemenKantin(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("MENYAPA")) {
                menyapa(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("ADD_MAKANAN")) {
                addMakanan(in.split(" ")[1], in.split(" ")[2], Long.parseLong(in.split(" ")[3]));
            } else if (in.split(" ")[0].equals("MEMBELI_MAKANAN")) {
                membeliMakanan(in.split(" ")[1], in.split(" ")[2], in.split(" ")[3]);
            } else if (in.split(" ")[0].equals("CREATE_MATKUL")) {
                createMatkul(in.split(" ")[1], Integer.parseInt(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_MATKUL")) {
                addMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("DROP_MATKUL")) {
                dropMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("MENGAJAR_MATKUL")) {
                mengajarMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("BERHENTI_MENGAJAR")) {
                berhentiMengajar(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MAHASISWA")) {
                ringkasanMahasiswa(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MATKUL")) {
                ringkasanMataKuliah(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("NEXT_DAY")) {
                nextDay();
            } else if (in.split(" ")[0].equals("PROGRAM_END")) {
                programEnd();
                break;
            }
        }
    }
}