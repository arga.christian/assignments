class Dosen extends ElemenFasilkom {


    private MataKuliah mataKuliah;

    public Dosen(String nama) {
    	/* constructor untuk elemen dosen */
    	super(nama);
    	this.tipe = "Dosen";
    	this.mataKuliah = null;
    }
    
    public MataKuliah getMataKuliah() {
    	return this.mataKuliah;
    }

    public void mengajarMataKuliah(MataKuliah mataKuliah) {
        /* mengecek nilai matakuliah, jika masih null artinya dosen dapat mengajar matakuliah tersebut */
    	if (this.mataKuliah == null) {
    		if (mataKuliah.getDosen() == null) {
    			this.mataKuliah = mataKuliah;
        		System.out.printf("%s mengajar mata kuliah %s\n", this.nama, this.mataKuliah);
        	/* jika matakuliah sudah memiliki dosen, maka dosen tidak dapat mengajar matakuliah tersebut */
    		} else {
    			System.out.printf("[DITOLAK] %s sudah memiliki dosen pengajar\n", mataKuliah);
    		}
    	/* jika tidak null, artinya dosen sudah mengajar di mata kuliah lain */
    	} else {
    		System.out.printf("[DITOLAK] %s sudah mengajar mata kuliah %s\n", this.nama, this.mataKuliah);
    	}
    }

    public void dropMataKuliah() {
        /* mengecek nilai matakuliah, jika tidak null maka nilai tersebut akan dijadikan null*/
    	if (!(this.mataKuliah == null)) {
    		System.out.printf("%s berhenti mengajar %s\n", this.nama, mataKuliah);
    		this.mataKuliah = null;
    	/* jika nilai matakuliah null, maka dosen sedang tidak mengajar matakuliah apapun */
    	} else {
    		System.out.printf("%s sedang tidak mengajar mata kuliah apapun\n", this.nama);
    	}
    }
}