class Makanan {

    private String nama;

    private long harga;

    public Makanan(String nama, long harga) {
        /* Constructor untuk makanan */
    	this.nama = nama;
    	this.harga = harga;
    }
    /* setter dan getter yang diperlukan untuk makanan */
    
    public String getNama() {
    	return this.nama;
    }
    
	public long getHarga() {
		return this.harga;
	}

    public String toString() {
        return this.nama;
    }

}