abstract class ElemenFasilkom {
   
    String tipe;
    
    String nama;

    int friendship;

    ElemenFasilkom[] telahMenyapa = new ElemenFasilkom[100];
    
    int jumlahDisapa;
    
    ElemenFasilkom(String nama) {
    	/* constructor untuk elemen fasilkom */
    	this.nama = nama;
    	this.tipe = "";
    	int friendship = 0;
    }
    
    void menyapa(ElemenFasilkom elemenFasilkom) {
        /* mengecek nilai elemen di dalam telah menyapa, jika masih null maka dapat ditambahkan dan elemen dapat disapa */
    	for (int i = 0; i < this.telahMenyapa.length; i++) {
    		if (this.telahMenyapa[i] == null) {
    			this.telahMenyapa[i] = elemenFasilkom;
    			jumlahDisapa++;
    			break;
    		/* jika yang ingin disapa sudah ada di dalam list, tidak melakukan apapun */
    		} else if (this.telahMenyapa[i].equals(elemenFasilkom)) break;
    	}
    }
    
    /* getter yang diperlukan untuk elemen fasilkom */
    
    String getNama() {
    	return this.nama;
    }
    
    String getTipe() {
    	return this.tipe;
    }
    
    ElemenFasilkom[] getTelahMenyapa() {
    	return this.telahMenyapa;
    }
    
    int getJumlahDisapa() {
    	return this.jumlahDisapa;
    }
    
    int getFriendship() {
    	return this.friendship;
    }

    void resetMenyapa() {
        /* mengecek apakah nilai elemen dalam list tidak null, jika tidak diubah menjadi null */
    	for (int i = 0; i < this.telahMenyapa.length; i++) {
    		if (!(this.telahMenyapa[i] == null)) {
    			this.telahMenyapa[i] = null;
    		}
    	} /* setelah dijadikan null, nilai jumlah disapa dijadikan 0 (direset) */ 
    	this.jumlahDisapa = 0;
    }

    void membeliMakanan(ElemenFasilkom pembeli, ElemenFasilkom penjual, String namaMakanan) {
        /* mengecek nilai namaMakanan terhadap list makanan yang dijual oleh penjual (dipastikan elemen kantin) */
    	ElemenKantin tryPenjual = (ElemenKantin) penjual;
    	for (int i = 0; i < tryPenjual.getDaftarMakanan().length; i++) {
    		/* jika nilainya null maka penjual tersebut tidak menjual namaMakanan */
    		if (tryPenjual.getDaftarMakanan()[i] == null) {
    			System.out.printf("[DITOLAK] %s tidak menjual %s\n", tryPenjual, namaMakanan);
    			break;
    		}
    		/* jika ditemukan, maka pembeli dapat membeli namaMakanan dari penjual */
    		else if (tryPenjual.getDaftarMakanan()[i].getNama().equals(namaMakanan)) {
    			System.out.printf("%s berhasil membeli %s seharga %d\n", pembeli, namaMakanan, tryPenjual.getDaftarMakanan()[i].getHarga());
    			pembeli.tambahFriendship(1);
    			penjual.tambahFriendship(1);
    			break;
    		}
    		/* jika nilainya tidak sama dengan namaMakanan, iterasi akan dilanjutkan apabila nilai i belum mengiterasi setiap makanan dalam list */
    		else if (!tryPenjual.getDaftarMakanan()[i].getNama().equals(namaMakanan)) {
    			if (i < tryPenjual.getDaftarMakanan().length) continue;
    			else if (tryPenjual.getDaftarMakanan()[i].getNama() != namaMakanan) {
    				System.out.printf("[DITOLAK] %s tidak menjual %s\n", pembeli, namaMakanan);
    			}
    		}
    	}
    }
    
    void tambahFriendship(int angka) {
    	/* method untuk menambahkan nilai friendship elemen, jika nilainya lebih besar dari 100
    	 * secara default akan dibuat menjadi 100 */
    	this.friendship += angka;
    	if (this.friendship > 100) this.friendship = 100;
    }
    
    void kurangFriendship(int angka) {
    	/* method untuk mengurangkan nilai friendship elemen, jika nilainya lebih kecil dari 0
    	 * secara default akan dibuat menjadi 0 */
    	this.friendship -= angka;
    	if (this.friendship < 0) this.friendship = 0;
    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        return this.nama;
    }
}