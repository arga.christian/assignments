class ElemenKantin extends ElemenFasilkom {

    private Makanan[] daftarMakanan = new Makanan[10];

    public ElemenKantin(String nama) {
        /* constructor untuk elemen kantin */
    	super(nama);
    	this.tipe = "Elemen Kantin";
    }
    
    /* setter dan getter yang diperlukan oleh elemen kantin */
    
    public Makanan[] getDaftarMakanan() {
    	return this.daftarMakanan;
    }

    public void setMakanan(String nama, long harga) {
        /* method untuk mengeset makanan, akan mengecek nilai makanan dalam list */
    	Makanan makanan = new Makanan(nama, harga);
    	for (int i = 0; i < daftarMakanan.length; i++) {
    		/* jika nilainya masih null, maka makanan akan dimasukkan ke dalam list */
    		if (daftarMakanan[i] == (null)) {
    			daftarMakanan[i]= makanan;
    			System.out.printf("%s telah mendaftarkan makanan %s dengan harga %d\n", this.nama, nama, harga);
    			break;
    		} 
    		/* jika sudah ada elemen yang sama dengan makanan di dalam list, maka makanan tidak akan dimasukkan */
    		else if (daftarMakanan[i].equals(makanan)) {
    			System.out.printf("[DITOLAK] %s sudah pernah terdaftar", makanan);
    		}
    	}
    }
}