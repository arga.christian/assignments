/*
Nama: Arga Christian Roymansa
NPM: 2006596554
KELAS: DDP2-C
KODE ASDOS = MYM
TP01-EXTRACTNPM
*/
package assignments.assignment1;

import java.util.Scanner;

public class ExtractNPM {
    /*
    You can add other method do help you solve
    this problem
    
    Some method you probably need like
    - Method to get tahun masuk or else
    - Method to help you do the validation
    - and so on
    */

    public static boolean validate(long npm) {
	 // TODO: validate NPM, return it with boolean
        String stringNpm = Long.toString(npm); //Mengubah long menjadi string
		if (stringNpm.length() == 14) { //mengecek jumlah string
		boolean check = yearsIn(stringNpm) && bachelorCode(stringNpm) && npmCode(stringNpm) && testMonth(stringNpm); //mengecek setiap tes validasi
		return check;
		} else {
			return false;
		} 
    }
    public static boolean yearsIn(String npm) {
    	int years = Integer.parseInt(npm.substring(0,2));
		if (years >= 10) { //mengecek tahun masuk dengan batas 2010
			return true;
		} else {
			return false;
		}
	}
	public static boolean testMonth(String npm) {
		int intMonth;
		if (npm.charAt(6) == '0') { //mengecek bulan lahir
			intMonth = Integer.parseInt(npm.substring(7, 8));
		} else {
			intMonth = Integer.parseInt(npm.substring(6, 8));
		}
		int intDate;
		if (npm.charAt(4) == '0') { //mengecek tanggal lahir
			intDate = Integer.parseInt(npm.substring (5,6));
		} else {
			intDate = Integer.parseInt(npm.substring(4, 6));
		}
		boolean firstCase = intMonth % 2 == 1 && intMonth < 8 && intDate <= 31;
		boolean secondCase = intMonth % 2 == 0 && intMonth < 8 && intDate <= 30;
		boolean thirdCase = intMonth % 2 == 1 && intMonth <= 12 && intDate <= 30;
		boolean forthCase = intMonth % 2 == 0 && intMonth <= 12 && intDate <= 31;
		if ( firstCase|| secondCase || thirdCase || forthCase) { //mengecek nilai kevalidan tanggal lahir
			return true;
		} else {
			return false;
		}
	}

    public static boolean bachelorCode(String npm) { //mengecek code jursuan
    	String bachelor = npm.substring(2, 4);
		boolean bach = bachelor.equals("01") || bachelor.equals("02") || bachelor.equals("03") || bachelor.equals("11") || bachelor.equals("12");
		return bach;
    }
    public static boolean npmCode(String npm) { //mengecek nomor terakhir dari npm
		int value = 0;
		for (int i = 0; i < npm.length() / 2 - 1; i++) {
			int num1 = Integer.parseInt(npm.substring(i, i + 1));
			int num2 = Integer.parseInt(npm.substring(npm.length() - 2 - i, npm.length() - 1 - i));
			int product = num1 * num2;
			value += product;
		}  value += Integer.parseInt(npm.substring(6,7));
		while (value >= 10) {
			String stringValue = Integer.toString(value);
			int number1 = Integer.parseInt(stringValue.substring(0,1));
			int number2 = Integer.parseInt(stringValue.substring(1));
			value = number1 + number2;
		}
		String valueString = Integer.toString(value);	
		return (npm.substring(13).equals(valueString));
    }
    
    public static String extract(long npm) { //mengeprint data tahun masuk, jurusan dan tanggal lahir.
        // TODO: Extract information from NPM, return string with given format
    	String stringNpm = Long.toString(npm);
		String year = "20".concat(stringNpm.substring(0, 2));
		String realBachelor = bachelor(stringNpm.substring(2, 4));
		String bornDate = stringNpm.substring(4, 6);
		String bornMonth = stringNpm.substring(6, 8);
		String bornYear = stringNpm.substring(8, 12);
		String formatYear = "Tahun masuk: " + year;
		String formatBachelor = "Jurusan: " + realBachelor;
		String formatBornDate = "Tanggal Lahir: " + bornDate + "-" + bornMonth + "-" + bornYear; 
		return formatYear + "\n" + formatBachelor + "\n" + formatBornDate;
    }

    public static String bachelor(String number) { //mengkategorikan jurusan dengan kodenya di npm
		String batch = "";
    	if (number.equals("01")) {
			batch = "Ilmu Komputer";
		} else if (number.equals("02")) {
			batch ="Sistem Informasi";
		} else if (number.equals("03")) {
			batch = "Teknologi Informasi";
		} else if (number.equals("11")) {
			batch = "Teknik Telekomunikasi";
		} else if (number.equals("12")) {
			batch = "Teknik Elektro";
		}
		return batch;
    }
    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
		long longNpm = 99010101200017L;
		String stringNpm = Long.toString(longNpm);
		boolean exitFlag = false;
		while (!exitFlag) {
		if (longNpm < 0) {
			System.out.println("NPM Tidak Valid");
			exitFlag = true;
		} else if (stringNpm.length() == 14) {
			if (validate(longNpm)) {
				System.out.println(extract(longNpm));
			} else {
				System.out.println("NPM Tidak Valid");
			}
            // TODO: Check validate and extract NPM
            
        }
	}
    	input.close();
    }
}